﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using WorkFlowApplication.Workflow;

namespace WorkFlowApplication.Data
{
    public class WorkflowService: DataContext
    {
        public WorkflowService()
            : base(ConfigurationManager.ConnectionStrings["InvoiceWorkFlow"].ConnectionString)
        {

        }
        

        public Table<WorkflowProcess> Workflow;
        public Table<Activity> Activity;
        public Table<Connection> Connections;
        public Table<WorkflowInstance> WorkflowInstance;
        public Table<WorkflowInstanceTrack> WorkflowInstanceTrack;
        public Table<ProcessActivityAttribute> ProcessActivityAttribute;
        public Table<WorkflowInstanceAttribute> WorkflowInstanceAttribute ;
        public Table<ActivityStatusRecord> InvoiceActivityTracking;
        public Table<Users> Users;
    }
}