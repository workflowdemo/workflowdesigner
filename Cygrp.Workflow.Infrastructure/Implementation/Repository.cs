﻿using Cygrp.Workflow.Core.Interface;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace WorkFlowApplication.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DataContext _context;
        public Repository(DataContext context)
        {
            _context = context;
        }
        public IEnumerable<T> GetAll()
        {
            return GetTable.ToList();
        }

        private System.Data.Linq.Table<T> GetTable
        {
            get { return _context.GetTable<T>(); }
        }

        public T GetBy(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> Query(Expression<Func<T, bool>> filter)
        {
            return GetTable.Where(filter);
        }

        public T Save(T entity)
        {
            GetTable.InsertOnSubmit(entity);

            _context.SubmitChanges();

            return entity;
        }

        public void Remove(T entity)
        {
            GetTable.DeleteOnSubmit(entity);
        }

        public void Save()
        {
            _context.SubmitChanges();
        }


        public void Add(T entity)
        {
            GetTable.InsertOnSubmit(entity);
        }


        public void Update(T entity)
        {  
          //  GetTable.Attach(entity);
        }
    }
}