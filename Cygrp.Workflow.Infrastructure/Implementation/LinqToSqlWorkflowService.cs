﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using WorkFlowApplication.Data;

namespace WorkFlowApplication.Repository
{
    public class LinqToSqlWorkflowService : IWorkflowService
    {
        private readonly DataContext _dataContext;
        private readonly IExpressionEvaluator _expressionEvaluator;

        public LinqToSqlWorkflowService(DataContext context, IExpressionEvaluator expressionEvaluator)
        {
            _dataContext = context;
            _expressionEvaluator = expressionEvaluator;
        }
        public IEnumerable<WorkflowProcess> GetAll()
        {
            return _dataContext.GetTable<WorkflowProcess>().ToList();
        }

        public void Save(WorkflowProcess process)
        {
            using (var ctx = new WorkflowService())
            {
                var existingWorkflowProcess = ctx.Workflow.Where(item => item.WorkflowProcessID.ToString().Equals(process.WorkflowProcessID.ToString())).FirstOrDefault();

                //update existing workflow
                if (existingWorkflowProcess != null)
                {
                    UpdateExistingWorkflow(process, ctx, existingWorkflowProcess);
                }
                else
                {
                    CreateNewWorkflow(process, ctx);
                }


                ctx.SubmitChanges();
            }
        }

        private static void UpdateExistingWorkflow(WorkflowProcess process, WorkflowService ctx, WorkflowProcess existingWorkflowProcess)
        {
            existingWorkflowProcess.Shapes = ctx.Activity.Where(i => i.WorkflowProcessID.Equals(existingWorkflowProcess.WorkflowProcessID)).ToList();
            if (existingWorkflowProcess.Shapes != null)
            {
                foreach (var shape in existingWorkflowProcess.Shapes)
                {
                    shape.Connections = ctx.Connections.Where(i => i.ShapeID.Equals(shape.Id)).ToList();
                }
            }

            //Update Existing Workflow
            existingWorkflowProcess.Name = process.Name;
            existingWorkflowProcess.Description = process.Description;


            //delete shapes and connections which is removed by user
            RemoveShapesAndConnections(process, ctx, existingWorkflowProcess);

            UpdateShapesAndConnections(process, ctx, existingWorkflowProcess);
        }

        private static void UpdateShapesAndConnections(WorkflowProcess process, WorkflowService ctx, WorkflowProcess existingWorkflowProcess)
        {
            foreach (Activity shape in process.Shapes)
            {
                //shape.WorkflowProcessID = process.WorkflowProcessID;
                var existingShape = existingWorkflowProcess.Shapes.FirstOrDefault(shp => shp.Id.Equals(shape.Id));
                shape.SetShapeType();

                if (existingShape != null)
                {
                    existingShape.Height = shape.Height;
                    existingShape.Width = shape.Width;
                    existingShape.positionX = shape.positionX;
                    existingShape.positionY = shape.positionY;
                    existingShape.Sequence = shape.Sequence;
                    existingShape.Text = shape.Text;
                    existingShape.TypeText = shape.TypeText;
                    existingShape.ExecutingUserType = shape.ExecutingUserType;

                    if (shape.Connections != null)
                    {
                        foreach (Connection conn in shape.Connections)
                        {
                            var existingConnection = ctx.Connections.FirstOrDefault(item => item.ConnectionID.Equals(conn.ConnectionID));

                            if (existingConnection != null)
                            {
                                existingConnection.ShapeID = shape.Id;
                                existingConnection.TargetId = conn.TargetId;
                                existingConnection.Text = conn.Text;
                            }
                            else
                            {
                                conn.ConnectionID = Guid.NewGuid();
                                conn.ShapeID = shape.Id;
                                ctx.Connections.InsertOnSubmit(conn);
                            }

                        }
                    }

                    if (shape.CustomAttributes != null)
                    {
                        foreach (ProcessActivityAttribute attribute in shape.CustomAttributes)
                        {
                            var existingAttribute = ctx.ProcessActivityAttribute.FirstOrDefault(item => item.Id.Equals(attribute.Id));

                            if (existingAttribute != null)
                            {
                                attribute.ActivityID = shape.Id;
                                attribute.WorkFlowProcessId = process.WorkflowProcessID;
                                attribute.AttributeType = shape.TypeText;
                            }
                            else
                            {
                                attribute.Id = Guid.NewGuid();
                                attribute.ActivityID = shape.Id;
                                attribute.WorkFlowProcessId = process.WorkflowProcessID;
                                attribute.AttributeType = shape.TypeText;
                                ctx.ProcessActivityAttribute.InsertOnSubmit(attribute);
                            }
                        }
                    }
                }
                else
                {
                    shape.WorkflowProcessID = existingWorkflowProcess.WorkflowProcessID;
                    if (!shape.ExecutingUserType.HasValue)
                    {
                        shape.ExecutingUserType = 'U';
                    }
                    ctx.Activity.InsertOnSubmit(shape);

                    if (shape.Connections != null)
                    {
                        foreach (Connection conn in shape.Connections)
                        {
                            conn.ConnectionID = Guid.NewGuid();
                            conn.ShapeID = shape.Id;
                            ctx.Connections.InsertOnSubmit(conn);
                        }
                    }

                    if (shape.CustomAttributes != null)
                    {
                        foreach (ProcessActivityAttribute attribute in shape.CustomAttributes)
                        {
                            attribute.Id = Guid.NewGuid();
                            attribute.ActivityID = shape.Id;
                            attribute.WorkFlowProcessId = process.WorkflowProcessID;
                            attribute.AttributeType = shape.TypeText;
                            ctx.ProcessActivityAttribute.InsertOnSubmit(attribute);
                        }
                    }
                }

            }
        }

        private static void RemoveShapesAndConnections(WorkflowProcess process, WorkflowService ctx, WorkflowProcess existingWorkflowProcess)
        {
            var result = existingWorkflowProcess.Shapes.Except<Activity>(process.Shapes);

            foreach (var existingShape in existingWorkflowProcess.Shapes)
            {
                if (process.Shapes.Where(shp => shp.Id.Equals(existingShape.Id)).FirstOrDefault() == null)
                {
                    var shapeConnections = ctx.Connections.Where(x => x.ShapeID.Equals(existingShape.Id)).ToList();
                    ctx.Connections.DeleteAllOnSubmit(shapeConnections);

                    var targetConnections = ctx.Connections.Where(x => x.TargetId.Equals(existingShape.Id)).ToList();
                    ctx.Connections.DeleteAllOnSubmit(targetConnections);

                    var customAttributes = ctx.ProcessActivityAttribute.Where(x => x.ActivityID.Equals(existingShape.Id)).ToList();
                    ctx.ProcessActivityAttribute.DeleteAllOnSubmit(customAttributes);

                    ctx.Activity.DeleteOnSubmit(existingShape);
                }
                else if (existingShape.Connections != null)
                {
                    var shp = process.Shapes.Where(x => x.Id.Equals(existingShape.Id)).FirstOrDefault();

                    foreach (Connection conn in existingShape.Connections)
                    {
                        if (shp != null
                            && (shp.Connections == null
                            || shp.Connections.Where(x => x.ConnectionID.Equals(conn.ConnectionID)).FirstOrDefault() == null))
                        {
                            ctx.Connections.DeleteOnSubmit(conn);
                        }
                    }
                }
            }
        }

        private static void CreateNewWorkflow(WorkflowProcess process, WorkflowService ctx)
        {
            //Save new workflow
            process.WorkflowProcessID = Guid.NewGuid();
            ctx.Workflow.InsertOnSubmit(process);
            foreach (Activity shape in process.Shapes)
            {
                shape.WorkflowProcessID = process.WorkflowProcessID;
                shape.Name = shape.Text;
                shape.SetShapeType();
                if (!shape.ExecutingUserType.HasValue)
                {
                    shape.ExecutingUserType = 'U';
                }
                ctx.Activity.InsertOnSubmit(shape);

                if (shape.Connections != null)
                {
                    foreach (Connection conn in shape.Connections)
                    {
                        conn.ConnectionID = Guid.NewGuid();
                        conn.ShapeID = shape.Id;
                        ctx.Connections.InsertOnSubmit(conn);
                    }
                }

                if (shape.CustomAttributes != null)
                {
                    foreach (ProcessActivityAttribute attribute in shape.CustomAttributes)
                    {
                        attribute.Id = Guid.NewGuid();
                        attribute.ActivityID = shape.Id;
                        attribute.WorkFlowProcessId = process.WorkflowProcessID;
                        //attribute.AttributeType = shape.TypeText;
                        ctx.ProcessActivityAttribute.InsertOnSubmit(attribute);
                    }
                }

            }
        }

        public WorkflowProcess Get(string name)
        {
            WorkflowProcess _process = _dataContext.GetTable<WorkflowProcess>().FirstOrDefault(i => i.Name.Equals(name));

            _process.Shapes = _dataContext.GetTable<Activity>().Where(i => i.WorkflowProcessID.Equals(_process.WorkflowProcessID)).ToList();
            if (_process.Shapes != null)
            {
                foreach (var shape in _process.Shapes)
                {
                    shape.Connections = _dataContext.GetTable<Connection>().Where(i => i.ShapeID.Equals(shape.Id)).ToList();
                    shape.CustomAttributes = _dataContext.GetTable<ProcessActivityAttribute>().Where(i => i.ActivityID.Equals(shape.Id)).ToList();
                }
            }

            return _process;
        }

        public Activity GetFirstProcessActivitiesBy(Guid workflowProcessId)
        {
            Activity _shape = null;

            var firstActivity = from activity in _dataContext.GetTable<Activity>()
                                join conn in _dataContext.GetTable<Connection>() on activity.Id equals conn.TargetId into gj
                                from subpet in gj.DefaultIfEmpty()
                                where activity.WorkflowProcessID.Equals(workflowProcessId)
                                where subpet == null
                                select new { Activity = activity };


            if (firstActivity.Any())
            {
                _shape = firstActivity.FirstOrDefault().Activity;
            }

            return _shape;
        }


        public Activity GetNextWorkflowActivity(Activity currentActivity, Guid workflowInstanceId)
        {
            Activity activity = null;


            var nextActivities = _dataContext.GetTable<Activity>().Join(_dataContext.GetTable<Connection>(), i => i.Id, j => j.ShapeID,
                    (act, con) => new { ActivityId = con.ShapeID, NextActivityId = con.TargetId }).Where(i => i.ActivityId.Equals(currentActivity.Id))
                    .ToList();

            if (!nextActivities.Any())
            {
                //END OF THE WORKFLOW
                return null;
            }

            if (nextActivities.Count().Equals(1))
            {
                activity = _dataContext.GetTable<Activity>().First(i => i.Id.Equals(nextActivities.First().NextActivityId));
            }
            else
            {
                //TODO: logic for evaluating activity route configuration
                var activitiesAttributes = _dataContext.GetTable<ProcessActivityAttribute>()
                    .Where(i => i.WorkFlowProcessId.Equals(currentActivity.WorkflowProcessID))
                    .Where(i => i.AttributeType.Equals("route"))
                    .Where(i => i.ActivityID.Equals(currentActivity.Id))
                    .ToList();

                var wfAttributes = _dataContext.GetTable<WorkflowInstanceAttribute>().Where(i => i.WorkflowInstanceId.Equals(workflowInstanceId));

                if (activitiesAttributes.Any())
                {
                    foreach (var actAttr in activitiesAttributes)
                    {
                        var evaluationString = actAttr.AttributeValue;

                        foreach (var instanceAttr in wfAttributes)
                        {
                            evaluationString = evaluationString.Replace(instanceAttr.AttributeKey, instanceAttr.AttributeValue);
                        }

                        var evaluationResult = _expressionEvaluator.Evaluate<string>(evaluationString);
                        if (evaluationResult.ToLower().Equals("true"))
                        {
                            activity = _dataContext.GetTable<Activity>().First(i => i.Id.Equals(actAttr.AttributeKey));
                            break;
                        }
                    }
                }
            }

            return activity;
        }

        public TimeSpan GetDelayTimeSpanForActivity(Activity fromActivityEntity, Guid workflowInstanceId)
        {
            var timeSpan = new TimeSpan();
            Double delayTimeDouble = 0d;


            // var value =
            var activitiesPropertyAttributes = _dataContext.GetTable<ProcessActivityAttribute>().Where(i => i.WorkFlowProcessId.Equals(fromActivityEntity.WorkflowProcessID))
                 .Where(i => i.ActivityID.Equals(fromActivityEntity.Id))
                 .Where(i => i.AttributeType.Equals("property"))
                 .Where(i => i.AttributeKey.Equals("@@DelayInSeconds")).Select(i => new { DelayInSeconds = i.AttributeValue }).ToList();

            if (activitiesPropertyAttributes.Any())
            {
                var delayInSecondsString = activitiesPropertyAttributes.FirstOrDefault().DelayInSeconds;

                if (delayInSecondsString.Contains("@@"))
                {
                    var workflowAttributes = _dataContext.GetTable<WorkflowInstanceAttribute>().Where(i => i.WorkflowInstanceId.Equals(workflowInstanceId));

                    var evaluationResult = delayInSecondsString;

                    foreach (var instanceAttr in workflowAttributes)
                    {
                        evaluationResult = evaluationResult.Replace(instanceAttr.AttributeKey, instanceAttr.AttributeValue);

                        var result = _expressionEvaluator.Evaluate(evaluationResult);
                        if (result != DateTime.MinValue)
                        {
                            timeSpan = result.Subtract(DateTime.Now);
                            return timeSpan;
                        }
                    }
                }
                else
                {
                    delayTimeDouble = double.Parse(delayInSecondsString);
                }

                timeSpan = TimeSpan.FromSeconds(delayTimeDouble);
            }
            return timeSpan;
        }



        public void StoreCustomAttributesfor(Guid workflowInstanceId, string activityId, string attributeKey, string attributeValue)
        {
            var activityEntity = _dataContext.GetTable<Activity>().Where(i => i.Id.Equals(activityId)).First();

            var atributes = _dataContext.GetTable<ProcessActivityAttribute>().Where(i => i.WorkFlowProcessId.Equals(activityEntity.WorkflowProcessID)).ToList();

            if (atributes.Any(i => i.AttributeValue.Contains(attributeKey)))
            {

                _dataContext.GetTable<WorkflowInstanceAttribute>()
                    .InsertOnSubmit(
                    new WorkflowInstanceAttribute
                    {
                        Id = Guid.NewGuid(),
                        ActivityAttributeId = Guid.NewGuid(),// need to be removed
                        AttributeValue = attributeValue,
                        AttributeKey = attributeKey,
                        WorkflowInstanceId = workflowInstanceId
                    });

                _dataContext.SubmitChanges();

            }
        }
    }
}