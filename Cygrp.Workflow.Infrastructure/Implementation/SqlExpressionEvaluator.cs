﻿using Cygrp.Workflow.Core.Interface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;

namespace WorkFlowApplication.Workflow
{
    public class SqlExpressionEvaluator:IExpressionEvaluator
    {
        private readonly DataContext dbContext;
        public SqlExpressionEvaluator(DataContext  context)
        {
            dbContext = context;
        }
        public T Evaluate<T>(string stringExpression) where T : class
        {
            if (typeof(T) == Type.GetType("System.String"))
            {
                return Evaluate1(stringExpression) as T;
            }

            return dbContext.ExecuteQuery<T>(stringExpression).FirstOrDefault();
        }

        private static string Evaluate1(string strToEvaluate)
        {
            return new DataTable().Compute(strToEvaluate, "").ToString();
        }


        DateTime IExpressionEvaluator.Evaluate(string stringExpression)
        {
            if (stringExpression.Contains("#"))
            {
                var strDate = stringExpression.Substring(stringExpression.IndexOf('#'), stringExpression.LastIndexOf('#') + 1);
                var strSqlDate = string.Format("select  cast('{0}' as datetime) ", strDate.Replace("#", ""));
               stringExpression = stringExpression.Replace(strDate, strSqlDate);
            }

            return dbContext.ExecuteQuery<DateTime>(stringExpression).FirstOrDefault();

        }
    }
}