﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Linq;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygrp.Workflow.Infrastructure
{
    public class QueryUserService:IQueryUserService
    {
        DataContext dataContext;

        string connectionString = ConfigurationManager.ConnectionStrings["InvoiceWorkFlow"].ConnectionString;
        public QueryUserService(DataContext context)
        {
            dataContext = context;
        }
        // WF - get user data and manager
        public Users GetUser(int userId)
        {
            return
               (from user in dataContext.GetTable<Users>()
                where user.UserId.Equals(userId)
                select new Users
                {
                    UserId = user.UserId,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    IsActive = user.IsActive,
                    TeamID = user.TeamID
                })
                    .FirstOrDefault();
        }

        // UI - Select employee
        public IList<Users> GetAllUsers()
        {
            return
               (from user in dataContext.GetTable<Users>()
                where user.UserId != 10000
                select new Users
                {
                    UserId = user.UserId,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    IsActive = user.IsActive,
                    TeamID = user.TeamID
                })
                    .ToList();
        }

    }
}
