﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Cygrp.Workflow.Infrastructure.Repository
{
    public class UserSessionRepository: IUserSessionRepository
    {
        private HttpSessionStateBase _sessionBase;
        IQueryUserService _queryUser;
        public UserSessionRepository(HttpSessionStateBase sessionBase, IQueryUserService queryUser)
        {
            _sessionBase = sessionBase;
            _queryUser = queryUser;
        }
        public Users GetLoggedInUser()
        {
            int userKey;
            if (_sessionBase["User"] != null)
            {
                 userKey = Convert.ToInt16(_sessionBase["User"]);
            }
            else
            {
                userKey = 6;// Ron Redmer
                _sessionBase["User"] = userKey;
            }
            return _queryUser.GetUser(userKey);
        }

        public  void SetUserSession(string userKey)
        {
            _sessionBase["User"] = userKey;
        }
    }
}