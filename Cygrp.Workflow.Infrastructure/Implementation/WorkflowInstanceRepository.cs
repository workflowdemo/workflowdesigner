﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using WorkFlowApplication.Data;
using WorkFlowApplication.Workflow;

namespace WorkFlowApplication.Repository
{
    public class WorkflowInstanceRepository : IWorkflowInstanceRepository
    {
        //public WorkflowInstance Get(string id)
        //{
        //    WorkflowInstance workflowInstance = null;

        //    using (var ctx = new WorkflowService())
        //    {
        //        workflowInstance = ctx.WorkflowInstance.Where(item => item.Id.ToString().Equals(id)).FirstOrDefault();
        //        if (workflowInstance != null)
        //        {
        //            workflowInstance.CurrentActivity = ctx.Activity.First(i => i.Id.Equals(workflowInstance.CurrentActivityId));
        //        }
        //    }

        //    return workflowInstance;
        //}

        public WorkflowInstance GetByReferenceId(string referenceId)
        {
            WorkflowInstance workflowInstance = null;

            using (var ctx = new WorkflowService())
            {
                workflowInstance = ctx.WorkflowInstance.Where(item => item.ReferenceId.ToString().Equals(referenceId)).FirstOrDefault();
            }

            return workflowInstance;
        }

        public void Save(WorkflowInstance workflowInstance)
        {
            using (var ctx = new WorkflowService())
            {
                ctx.WorkflowInstance.InsertOnSubmit(workflowInstance);
                ctx.SubmitChanges();
            }
        }

        public void Update(WorkflowInstance workflowInstance)
        {
            using (var ctx = new WorkflowService())
            {
                var instance = ctx.WorkflowInstance.ToList().Where(item => item.Id.ToString().Equals(workflowInstance.Id.ToString())).First();

                instance.IsStarted = workflowInstance.IsStarted;
                instance.Action = workflowInstance.Action;
                instance.ReferenceId = workflowInstance.ReferenceId;
                instance.CurrentActivityId = workflowInstance.CurrentActivityId;
                instance.IsEnded = workflowInstance.IsEnded;
                ctx.SubmitChanges();
            }
        }
    }
}