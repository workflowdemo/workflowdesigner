﻿//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using WorkFlowApplication.Models;
//using WorkFlowApplication.Repository;
//using WorkFlowApplication.Workflow;
//using WorkFlowApplication.Workflow.Extensions;
//using WorkFlowApplication.Extensions;
//using Invoice.Model;

//namespace WorkFlowApplication.Test
//{
//    [TestFixture]
//    public class WorkflowTest
//    {
//        Guid invoiceWorkflowProcessId;

//        [OneTimeSetUp]
//        public void Setup()
//        {
//            invoiceWorkflowProcessId = Guid.NewGuid();
//            CacheRepository<Activity> activityRepo = new CacheRepository<Activity>();
//            var startActivity = new Activity { Text = "Start", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };
//            var InvoiceCreatedActivity = new Activity { Text = "InvoiceCreated", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };
//            var AccountsApprovedActivity = new Activity { Text = "AccountsApproved", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };
//            var DeliveredToClientActivity = new Activity { Text = "DeliveredToClient", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };
//            var FollowUpActivity = new Activity { Text = "FollowUp", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };
//            var EndActivity = new Activity { Text = "End", Id = Guid.NewGuid().ToString(), WorkflowProcessID = invoiceWorkflowProcessId };


//            activityRepo.Save(startActivity, startActivity.Id);
//            activityRepo.Save(InvoiceCreatedActivity, InvoiceCreatedActivity.Id);
//            activityRepo.Save(AccountsApprovedActivity, AccountsApprovedActivity.Id);
//            activityRepo.Save(DeliveredToClientActivity, DeliveredToClientActivity.Id);
//            activityRepo.Save(FollowUpActivity, FollowUpActivity.Id);
//            activityRepo.Save(EndActivity, EndActivity.Id);

//        }

//        [TestCase]
//        public void Test01()
//        {


//            var instance = Orchestrator.InitialiseWorkflow(invoiceWorkflowProcessId,new Users());
//            instance.ReferenceId = "Invoice001";
//            instance.Action = "Invoice Initiated";
//            instance.Execute(); // Invoice Created

//           var savedInstance = Orchestrator.GetWorkflow(instance.Id);
//           savedInstance.Action = "Invoice Approved";
//           savedInstance.Execute();

//           Assert.AreEqual(2, new CacheRepository<WorkflowInstanceTrack>().GetAll().Count());

//           var invoiceApprovedInstance = Orchestrator.GetWorkflow(instance.Id);
//           invoiceApprovedInstance.Action = "Delivered to client";
//           invoiceApprovedInstance.Execute();

//           Assert.AreEqual(3, new CacheRepository<WorkflowInstanceTrack>().GetAll().Count());

//           var deliveredInstance = Orchestrator.GetWorkflow(instance.Id);
//           deliveredInstance.Action = "Followup";
//           deliveredInstance.Execute();

//           Assert.AreEqual(4, new CacheRepository<WorkflowInstanceTrack>().GetAll().Count());

//           var stopInstance = Orchestrator.GetWorkflow(instance.Id);
//           stopInstance.Action = "ENd";
//           stopInstance.Execute();

//           Assert.AreEqual(5, new CacheRepository<WorkflowInstanceTrack>().GetAll().Count());


//        }
//    }
//}
