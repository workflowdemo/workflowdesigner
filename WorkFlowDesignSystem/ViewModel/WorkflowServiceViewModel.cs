﻿using Cygrp.Workflow.Core.Model;
using Cygrp.Workflow.Web.DbContext;
using Cygrp.Workflow.Web.EntityModel;
using Cygrp.Workflow.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WorkFlowApplication.Data;

namespace WorkFlowApplication.Models
{
   
    public class WorkflowServiceViewModel
    {
        private AppDbContext _workflowService = new AppDbContext();

        public int NoOfRunningWorkflowInstance { get; set; }

        public int InvoiceCompleted { get; set; }

        public int InvoicePending { get { return _workflowService.InvoiceRequest.Count(i => (i.IsSubmittedToAccountIn && !i.IsDeliveredToClient)); } }

        public int InvoiceRaised { get; set; }

        public IEnumerable<WorkflowInstanceTrack> InvoiceWorkflowInstances { get; set; }

        public IEnumerable<InvoiceRequest> InvoiceRequests { get { return _workflowService.InvoiceRequest.OrderByDescending(i => i.DueDate).Take(10); } }
       
    }

    public class KeyValueData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    
    public class InstanceActivityList
    {
        public string InstanceId { get; set; }
        public string ProcessName { get; set; }
        public string ProcessReferenceId { get; set; }
        public DateTime EvntDate { get; set; }
        public string LastState { get; set; }
        public string LastProcessState { get; set; }
    }
    public class InvoiceactivitytrackingList
    {
        public string InstanceId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal Amount { get; set; }
        public string AcitivityName { get; set; }
        public string CreatedBy { get; set; }
        public string Assigneduser { get; set; }

        public int CompleteduserId { get; set; }
        public string Completeduser { get; set; }        
        public string ClientName { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public DateTime? AssignedOn { get; set; }
        public string AssignedTo { get; set; }
        public string CompletedTime {
            get {
                if (CompletedDate != null)
                    return ((TimeSpan)(CompletedDate - CreatedDate)).TotalSeconds.ToString("F0");
                else
                    return string.Empty;
            }
        }        
    }
}