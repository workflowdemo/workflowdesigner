﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkFlowApplication.ViewModel
{
    public class GetAverageTimeTakenByUsersViewModel
    {
        public int? UserId { get; set; }
        public bool ForAll { get; set; }
        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}