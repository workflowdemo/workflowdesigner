﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorkFlowApplication.ViewModel
{
    public class AssignedRequestViewModel
    {
        public string RequestId { get; set; }
        public string RequestType { get; set; }
        public string RequestDetails { get; set; }
        public string  RequestProcessReferenceId { get; set; }
        public string AssignedBy { get; set; }
        public string AssignedByUserId { get; set; }
        public string RequestedAction { get; set; }
        public DateTime DueDate { get; set; }
        public string Status { get; set; }

    }
}