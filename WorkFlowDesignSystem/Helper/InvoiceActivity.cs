﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygrp.Workflow.Web.Helper
{
    public class InvoiceActivity
    {
        private static Dictionary<int, string> activityType = new Dictionary<int, string>();

        public static Dictionary<int, string> ActivityType
        {
            get
            {
                if (!activityType.Count.Equals(7)) {
                    activityType.Add(1, "Create New Invoice");
                    activityType.Add(2, "Submit Invoice to Acounting");
                    activityType.Add(3, "Submit Invoice to Client");
                    activityType.Add(4, "Followup For Payment");
                    activityType.Add(5, "Invoice Response Required");
                    activityType.Add(6, "Write Off balance");
                    activityType.Add(7, "Zero Balance");
                }  
                return activityType;
            }
        }
    }

    public class InvoiceClients
    {
        private static Dictionary<int, string> clientsName = new Dictionary<int, string>();

        public static Dictionary<int, string> ClientName
        {
            get
            {
                if (!clientsName.Count.Equals(5))
                {
                    clientsName.Add(1, "Chase");
                    clientsName.Add(2, "Wells Fargo");
                    clientsName.Add(3, "Bank Of America");
                    clientsName.Add(4, "Federal Bank");
                    clientsName.Add(5, "Another Bank");
                 }
                return clientsName;
            }
        }
    }
}
