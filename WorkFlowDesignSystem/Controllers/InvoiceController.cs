﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkFlowApplication.Repository;
using WorkFlowApplication.Workflow;
using WorkFlowApplication.Data;
using WorkFlowApplication.ViewModel;
using WorkFlowApplication.Models;
using Cygrp.Workflow.Web.Helper;
using Cygrp.Workflow.Core;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Extensions;
using Cygrp.Workflow.Web.EntityModel;
using Cygrp.Workflow.Web.DbContext;
using Cygrp.Workflow.Core.Model;

namespace WorkFlowApplication.Controllers
{
    public class InvoiceController : Controller
    {
        WorkflowApplication workflowApplication;
        IUserSessionRepository _userSessionRepository;
        IQueryUserService _queryUserService;

        public InvoiceController(WorkflowApplication workflowApp, IUserSessionRepository userSessionRepository, IQueryUserService queryUserService)
        {
            workflowApplication = workflowApp;
            _userSessionRepository = userSessionRepository;
            _queryUserService = queryUserService;
        }

        public ActionResult Index()
        {
            var users = _queryUserService.GetAllUsers().ToList();
            return View("newinvoice", users);
        }

        [HttpPost]
        public ActionResult NewInvoice(InvoiceRequest request)
        {
            var loggedInUser = _userSessionRepository.GetLoggedInUser();
            workflowApplication.InitialiseWorkflow(Guid.Parse("36bb4e3c-8184-4786-a2f7-64b720410cb9"), loggedInUser);
            
            var invoiceRequest = new InvoiceRequest
            {
                InvoiceId = Guid.NewGuid(),
                ActivityType = 1,
                Amount = request.Amount,
                Description = request.Description,
                InvoiceNumber = request.InvoiceNumber,
                WorkflowInstanceId = workflowApplication.Instance.Id,
                DueDate = DateTime.Today.AddDays(2),
                ClientFollowupTime = DateTime.Today.AddDays(5).TimeOfDay,
                CreatedBy = loggedInUser.UserId.ToString(),
                CreationDate = DateTime.Today,
                ReferenceClientId = request.ReferenceClientId,
                ReferenceFileId = "0000000000000021",
                ReferenceProcessType = 1, // Invoice Process
                CreatedByUserId = loggedInUser.UserId,
                AssignedUserId = request.AssignedUserId,
                EscalationDays = new Random().Next(1, 3),
                Comments = "Invoice Created New!!",
                IsSubmittedToAccountIn = true
            };

            InvoiceRequestRepository.SaveInvoiceRequest(invoiceRequest);
            workflowApplication.Instance.Action = "Invoice Created";
            workflowApplication.Instance.ReferenceId = invoiceRequest.InvoiceId.ToString();

            workflowApplication.Instance.SetActivityStatus(new ActivityStatusRecord
                                {
                                    DueDate = invoiceRequest.DueDate,
                                    CreatedByUserId = loggedInUser.UserId,
                                    AssignedToUserId = request.AssignedUserId,
                                    ActionTaken = 1
                                });

            workflowApplication.Run();

            return RedirectToAction("Index", "Home");
        }
        public void ExecuteCourtAppointment(WorkflowInstance instance, string CourtAppointmentId, int AssignedtoUserId, string RequestTypeIs = null)
        {

            CourtAppointment courtappointment = null;
            using (var ctx = new AppDbContext())
            {
                courtappointment = ctx.CourtAppointment.Where(item => item.Id.ToString().Equals(CourtAppointmentId)).FirstOrDefault();
            }
            if (instance.Action.Equals("Court Appointment Created"))
            {
                instance.Action = "Court Appointment Completed";

            }
            else
            {
               
                instance.Action = " completed.";
            }
           

        }
        public void ExecuteInvoiceRequest(WorkflowInstance instance, string InvoiceId, int AssignedtoUserId, string followupForPayment = null)
        {
            InvoiceRequest invoiceRequest = null;
            using (var ctx = new AppDbContext())
            {
                invoiceRequest = ctx.InvoiceRequest.Where(item => item.InvoiceId.ToString().Equals(InvoiceId)).FirstOrDefault();
            }

            if (instance.Action.Equals("Invoice Created"))
            {
                instance.Action = "Accounts Approved";

            }
            else if (instance.Action.Equals("Accounts Approved"))
            {
                instance.Action = "Submitted to client";

                invoiceRequest.IsDeliveredToClient = true;

            }
            else if (instance.Action.Equals("Submitted to client"))
            {
                workflowApplication.SetAttribute(instance, "@@Response", followupForPayment);

                var action = followupForPayment.ToLower().Equals("approved") ? "Payment received" : followupForPayment.ToLower().Equals("invalid") ? "invalid invoice" : "query from client";
                instance.Action = action;

                if (followupForPayment.ToLower().Equals("approved") || followupForPayment.ToLower().Equals("invalid"))
                {
                    // complete request.
                    invoiceRequest.IsFollowUpCompleted = true;
                    invoiceRequest.IsCompleted = true;
                }

            }
            else
            {
                instance.Action = " completed.";
            }
        }

        public ActionResult IsApproved(string InvoiceId,int AssignedtoUserId, string followupForPayment = null, string RequestTypeIs = null)
       {
           var loggedInUser = _userSessionRepository.GetLoggedInUser();
           var instance =  workflowApplication.GetWorkflowInstance(InvoiceId);
          if (!string.IsNullOrEmpty(RequestTypeIs) && RequestTypeIs == "Court Appointment")
            {
                ExecuteCourtAppointment(instance, InvoiceId, AssignedtoUserId, RequestTypeIs);
            }
            else
            {
                ExecuteInvoiceRequest(instance, InvoiceId, AssignedtoUserId, followupForPayment);
            }
          

           instance.SetActivityStatus(new ActivityStatusRecord
           {
               DueDate = DateTime.Now,
               CreatedByUserId = loggedInUser.UserId,
               AssignedToUserId = AssignedtoUserId,
               Comments = instance.Action
           });

           workflowApplication.Run();

        

           return Json(InvoiceId);
       }

       public ActionResult ViewInvoices()
       {
           var loggedInUser = _userSessionRepository.GetLoggedInUser();
           var invoiceAssignedToMe = GetAssignedRequest(loggedInUser.UserId);//new DatabaseRepository().GetInvoiceRequestsAssignedToMe(loggedInUser.UserId.ToString());
           var users = _queryUserService.GetAllUsers().ToList();
           var activity = InvoiceActivity.ActivityType;
           var clients = InvoiceClients.ClientName;
           //ViewBag._listInvoice = invoiceAssignedToMe;
           ViewBag._listUsers = users;
         //  ViewBag._listActivity = activity;
         //  ViewBag._listclientName = clients;

           return View("viewinvoices", invoiceAssignedToMe);
       }


       private IEnumerable<AssignedRequestViewModel> GetAssignedRequest(int userId)
       {

           /*
            * select wat.WorkflowInstanceID,wat.ReferenceProcessID, wp.Name, wat.ActivityName, wat.AssignedUser, wat.CompletedUser, wat.CompletedDate, wf.IsEnded
from WorkflowActivityTracking wat inner join WorkflowInstance wf on wf.Id = wat.WorkflowInstanceID and wf.IsEnded=0
inner join WorkflowProcesses wp on wf.ProcessTypeId = wp.WorkflowProcessID
where wat.CompletedDate is null and wat.AssignedUser=1;
            */
           var assignedRequests = new List<AssignedRequestViewModel>();

            using (var ctx = new WorkflowService())
           {
               assignedRequests = ctx.InvoiceActivityTracking
                  .Join(ctx.WorkflowInstance, i => i.WorkFlowInstanceID, i => i.Id, (track, wf) => new { wf, track })
                  .Join(ctx.Workflow, i => i.wf.ProcessTypeId, i => i.WorkflowProcessID, (wfI, wfp) => new { wfI, wfp })
                  .Join(ctx.Users, i => i.wfI.track.AssignedToUserId, i => i.UserId, (wf, usr) => new { wf, usr })
                  .Join(ctx.Activity, i => i.wf.wfI.track.ActivityId, i => i.Id, (wfu, act) => new { wf = wfu.wf, usr = wfu.usr, act })
                  .Where(i => !i.wf.wfI.track.CompletedDate.HasValue)
                  .Where(i => i.wf.wfI.track.AssignedToUserId.Equals(userId))
                  .Select(i => new AssignedRequestViewModel
                  {
                      DueDate = i.wf.wfI.track.DueDate,
                      RequestProcessReferenceId = i.wf.wfI.wf.ReferenceId,
                      RequestType = i.wf.wfp.Name,
                      RequestId = i.wf.wfI.track.WorkFlowInstanceID.ToString(),
                      AssignedByUserId = i.wf.wfI.track.AssignedByUserId.ToString(),
                      AssignedBy = i.usr.FullName,
                      RequestedAction = i.act.Text,
                      RequestDetails = i.act.Text, Status="Details"
                  }).ToList();
            
            }
            return assignedRequests;
       }

    }
}
