﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkFlowApplication.Models;
using WorkFlowApplication.Repository;
using WorkFlowApplication.Workflow;
using WorkFlowApplication.Data;
using Cygrp.Workflow.Core;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Extensions;
using Cygrp.Workflow.Web.DbContext;
using Cygrp.Workflow.Core.Model;
using Cygrp.Workflow.Web.EntityModel;

namespace WorkFlowApplication.Controllers
{
    public class CourtAppointmentController : Controller
    {
        //
        // GET: /CourtAppointment/
        WorkflowApplication workflowApplication;
        IUserSessionRepository _userSessionRepository;
        IQueryUserService _queryUserService;

        public CourtAppointmentController(WorkflowApplication workflowApp, IUserSessionRepository userSessionRepository, IQueryUserService queryUserService)
        {
            workflowApplication = workflowApp;
            _userSessionRepository = userSessionRepository;
            _queryUserService = queryUserService;
        }

        public ActionResult Index()
        {
            var users = _queryUserService.GetAllUsers().ToList();
            return View("Index", users);
            
        }
        [HttpPost]
        public ActionResult Index(CourtAppointment CourtAppointmentRequest)
        {
            var loggedInUser = _userSessionRepository.GetLoggedInUser();
            workflowApplication.InitialiseWorkflow(Guid.Parse("3f755eb7-4c98-49d4-975e-78d9e4605bae"), loggedInUser);
            var CourtAppointments = new CourtAppointment();
            CourtAppointments.Id = Guid.NewGuid();
            CourtAppointments.CourtAppointmentDate = CourtAppointmentRequest.CourtAppointmentDate;
            CourtAppointments.PreparationDate = CourtAppointmentRequest.CourtAppointmentDate.AddDays(-10);
            CourtAppointments.IsDocumentPrepared = false;
            CourtAppointments.IsSendReminder = false;
            CourtAppointments.ReportToClientDate = null;
            CourtAppointments.IsReportToClient = false;
            CourtAppointments.CreatedOn = DateTime.Now;
            CourtAppointments.CreatedBy = loggedInUser.UserId;
            CourtAppointments.AssignedTo = CourtAppointmentRequest.AssignedTo;
            using (var dbContext = new AppDbContext())
              {
               dbContext.CourtAppointment.InsertOnSubmit(CourtAppointments);
                dbContext.SubmitChanges();
              }
            workflowApplication.Instance.Action = "Court Appointment Created";
            workflowApplication.Instance.ReferenceId = CourtAppointments.Id.ToString();

            workflowApplication.SetAttribute(workflowApplication.Instance,"@@CourtAppointmentDate", "#" + CourtAppointmentRequest.CourtAppointmentDate.ToString("dd/MMM/yyyy hh:mm") + "#");

            workflowApplication.Instance.SetActivityStatus(new ActivityStatusRecord
            {
                DueDate =DateTime.Now,
                CreatedByUserId = loggedInUser.UserId,
                AssignedToUserId = (int)CourtAppointmentRequest.AssignedTo,
                ActionTaken = 1
            });

            workflowApplication.Run();

            return RedirectToAction("Index", "Home");

        }

    }
}
