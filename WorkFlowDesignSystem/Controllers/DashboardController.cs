﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using Cygrp.Workflow.Web.DbContext;
using Cygrp.Workflow.Web.Helper;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorkFlowApplication.Data;
using WorkFlowApplication.Models;
using WorkFlowApplication.Repository;
using WorkFlowApplication.ViewModel;

namespace WorkFlowApplication.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        private readonly DataContext _workflowDbService;

        public DashboardController(DataContext workflowDbService)
        {
            _workflowDbService = workflowDbService;
        }

        public ActionResult Index()
        {            
            WorkflowServiceViewModel model = new WorkflowServiceViewModel();
            model.InvoiceWorkflowInstances = _workflowDbService.GetTable<WorkflowInstanceTrack>().OrderByDescending(w => w.EventDate).Take(10);
            ViewBag.AverageInstanceDetailsByUser = AverageInstanceDetailsByUser(null);
            ViewBag.AverageInstanceDetailsByActivity = AverageInstanceDetailsByActivity(null);
            ViewBag.Activities = Activities.Where(a => !a.Sequence.Equals(5)).OrderBy(o => o.Sequence);
            ViewBag.JsonData = Newtonsoft.Json.JsonConvert.SerializeObject(ViewBag.AverageInstanceDetailsByUser);
            return View(model);
        }


        private IEnumerable<KeyValueData> AverageInstanceDetailsByUser(int? user = null)
        {

            var completedActivitiesQuery = _invoiceactivitytrackingList
                .Where(p => p.CompletedDate != null)
                .Where(p => !string.IsNullOrEmpty(p.Completeduser));

            if (user.HasValue)
            {
                completedActivitiesQuery = completedActivitiesQuery.Where(p => p.CompleteduserId.Equals(user));
            }

            var selectAverakeTimeTakenByActivity = completedActivitiesQuery.Select(p => new
            {
                Diff = p.CompletedDate - p.CreatedDate,
                ActivityName = p.AcitivityName
            }).GroupBy(g => g.ActivityName)
                        .Select(i => new KeyValueData
                        {
                            Value = i.Average(o => o.Diff.Value.TotalSeconds).ToString("F0"),
                            Key = i.Key
                        });

            return selectAverakeTimeTakenByActivity;
        }

        public List<InstanceActivityList> _instanceActivityList
        {
            get
            {
                var dbContext = new AppDbContext();

                return (from wfi in _workflowDbService.GetTable<WorkflowInstance>().ToList()
                        join ir in dbContext.InvoiceRequest.ToList() on wfi.ReferenceId equals ir.InvoiceId.ToString()
                        join wfp in _workflowDbService.GetTable<WorkflowProcess>().ToList() on wfi.ProcessTypeId equals wfp.WorkflowProcessID
                        join wfit in _workflowDbService.GetTable<WorkflowInstanceTrack>().ToList() on wfi.Id equals wfit.WorkflowInstanceId
                        select new InstanceActivityList
                        {
                            InstanceId = wfi.Id.ToString(),
                            ProcessReferenceId = wfp.WorkflowProcessID.ToString(),
                            ProcessName = wfp.Name,
                            EvntDate = wfit.EventDate,
                            LastState = wfi.Action,
                            LastProcessState = null
                        }).ToList();
            }
        }
        public List<InvoiceactivitytrackingList> _invoiceactivitytrackingList
        {
            get
            {
                var dbContext = new AppDbContext();

                List<SelectListItem> clientsList = new List<SelectListItem>();
                var clients = InvoiceClients.ClientName;
                foreach (var list in clients)
                {
                    clientsList.Add(new SelectListItem() { Text = list.Value, Value = list.Key.ToString() });
                }
                var totalList = (from wfi in _workflowDbService.GetTable<WorkflowInstance>().ToList()
                                 join ir in dbContext.InvoiceRequest.ToList() on wfi.ReferenceId equals ir.InvoiceId.ToString()
                                 join iat in _workflowDbService.GetTable<ActivityStatusRecord>().ToList() on ir.WorkflowInstanceId equals iat.WorkFlowInstanceID
                                 join usr in _workflowDbService.GetTable<Users>().ToList() on iat.AssignedToUserId equals usr.UserId
                                 join usr1 in _workflowDbService.GetTable<Users>().ToList() on iat.CreatedByUserId equals usr1.UserId
                                 join usrCompleted in _workflowDbService.GetTable<Users>().ToList() on iat.CompletedUserId equals usrCompleted.UserId
                                 select new InvoiceactivitytrackingList
                                 {
                                     InstanceId = wfi.Id.ToString(),
                                     InvoiceNumber = ir.InvoiceNumber,
                                     Amount = ir.Amount,
                                     AcitivityName = iat.ActivityName,
                                     CreatedBy = usr1.FullName,
                                     Assigneduser = usr.FullName,
                                     ClientName = ir.ReferenceClientId,
                                     Completeduser = usrCompleted.FullName,
                                     CompleteduserId = iat.CompletedUserId.Value,
                                     CreatedDate = iat.CreatedDate,
                                     CompletedDate = iat.CompletedDate,
                                     AssignedOn = iat.AssignedDate,
                                     AssignedTo = usrCompleted.FullName

                                 }).OrderByDescending(iat => iat.CreatedDate).ToList();

                var NewList = totalList.Join(clientsList, t1 => t1.ClientName, c1 => c1.Value, (t1, c1) => new InvoiceactivitytrackingList
                {
                    InstanceId = t1.InstanceId,
                    InvoiceNumber = t1.InvoiceNumber,
                    Amount = t1.Amount,
                    AcitivityName = t1.AcitivityName,
                    CreatedBy = t1.CreatedBy,
                    Assigneduser = t1.Assigneduser,
                    Completeduser = t1.Completeduser,
                    CompleteduserId = t1.CompleteduserId,
                    ClientName = c1.Text,
                    CreatedDate = t1.CreatedDate,
                    CompletedDate = t1.CompletedDate,
                    AssignedOn = t1.AssignedOn,
                    AssignedTo = t1.AssignedTo
                }).ToList();
                return NewList;
            }
        }

        public IEnumerable<KeyValueData> AverageInstanceDetails
        {
            get
            {
                var lst = (from p in _invoiceactivitytrackingList
                           where p.CompletedDate != null
                           select new
                           {
                               Diff = p.CompletedDate - p.CreatedDate,
                               ActivityName = p.AcitivityName
                           }
                           );

                var avgInstanceDetails = lst
                    .GroupBy(g => g.ActivityName)
                    .Select(i => new KeyValueData
                    {
                        Value = i.Average(o => o.Diff.Value.TotalSeconds).ToString("F0"),
                        Key = i.Key
                    });

                return avgInstanceDetails;
            }
        }


        public IEnumerable<KeyValueData> AverageInstanceDetailsByActivity(string activity = null)
        {
            var completedActivitiesQuery = _invoiceactivitytrackingList
                .Where(p => p.CompletedDate != null)
                .Where(p => !string.IsNullOrEmpty(p.Completeduser));

            if (activity != null)
            {
                completedActivitiesQuery = completedActivitiesQuery.Where(p => p.AcitivityName.Equals(activity));
            }

            var avgInstanceDetails = completedActivitiesQuery
                .Select(p => new
                {
                    Diff = p.CompletedDate - p.CreatedDate,
                    User = p.Completeduser
                })
                .GroupBy(g => g.User)
                .Select(i => new KeyValueData
                {
                    Value = i.Average(o => o.Diff.Value.TotalSeconds).ToString("F0"),
                    Key = i.Key
                });

            return avgInstanceDetails;
        }

        public IEnumerable<Activity> Activities
        {
            get { return _workflowDbService.GetTable<Activity>() ; }
        }

        public ActionResult InstanceActivity()
        {
            WorkflowServiceViewModel model = new WorkflowServiceViewModel();
            model.NoOfRunningWorkflowInstance = _workflowDbService.GetTable<WorkflowInstance>().Count(i => i.IsStarted && !i.IsEnded);
            model.InvoiceCompleted = _workflowDbService.GetTable<WorkflowInstance>().Count(i => i.IsEnded); 

            model.InvoiceRaised =_workflowDbService.GetTable<WorkflowInstance>().Count(i => i.IsStarted);

            model.InvoiceWorkflowInstances =_workflowDbService.GetTable<WorkflowInstanceTrack>().OrderByDescending(w => w.EventDate).Take(10);
            return View(model);
           
        }

        public ActionResult InstanceDetail(string id)
        {
            ViewBag.InstanceDetails = _invoiceactivitytrackingList.Where(i => i.InstanceId.Equals(id.ToUpper())).OrderByDescending(i => i.CompletedDate);
            ViewBag.AverageInstanceDetails = AverageInstanceDetails;            
            return View();
        }

        public JsonResult GetAverageTimeTakenByUsers(GetAverageTimeTakenByUsersViewModel model)
        {

            var result = AverageInstanceDetailsByUser(model.ForAll ? null : model.UserId);
            return Json(
                new
                {
                    activity = result.Select(i => i.Key).ToList(),
                    timeTaken = result.Select(i => i.Value).ToList()
                }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAverageTimeTakenOnActivity(GetAverageTimeTakenOnActivityViewModel model)
        {
            var result = AverageInstanceDetailsByActivity(model.ForAll ? null : model.ActivityId);
            return Json(
                new
                {
                    users = result.Select(i => i.Key).ToList(),
                    timeTaken = result.Select(i => i.Value).ToList()
                }, JsonRequestBehavior.AllowGet);
        }
        
    }
}
