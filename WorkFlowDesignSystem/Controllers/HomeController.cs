﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Web.UI.WebControls;
using System.Text;
using WorkFlowApplication.Models;
using WorkFlowApplication.Repository;
using System.Data.Entity;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;

namespace WorkFlowApplication.Controllers
{
    public class HomeController : Controller
    {
        //
        IWorkflowService _workflowService;
        IRepository<Users> userRepository;
        IUserSessionRepository _userSessionRepository;

        public HomeController(IWorkflowService workflowService, IRepository<Users> userrepo, IUserSessionRepository userSessionRepository)
        {
            _workflowService = workflowService;
            userRepository = userrepo;
            _userSessionRepository = userSessionRepository;
        }

        public ActionResult Index()
        {
            return View("newindex");
        }
        public ActionResult NewProcedure()
        {
            return View("index");
        }
        public ActionResult GetProcess(string name)
        {
            var workflowProcess = _workflowService.Get(name);
            return Json(workflowProcess);
        }

        public ActionResult BindFileNamesToList()
        {
            return Json(_workflowService.GetAll().Select(i => new { Name = i.Name }), JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveWorkflow(WorkflowProcess workflow)
        {
            _workflowService.Save(workflow);

            return Json(new { Result = workflow });
        }

        public JsonResult GetAllUsers() {
            return Json(userRepository.GetAll(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SetUser(string userKey)
        {
            _userSessionRepository.SetUserSession(userKey);
            return RedirectToAction("Index");
        }
    }
}
