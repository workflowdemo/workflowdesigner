﻿using Autofac.Core;
using Autofac.Integration.Mvc;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;

namespace WorkFlowApplication.Extensions
{
    public static class ApplicationExtensions
    {
        public static string LoggedInUser(this HtmlHelper helper)
        {
          var dependencyResolver =  DependencyResolver.Current as AutofacDependencyResolver;
        var userSessionRepository =  dependencyResolver.GetService<IUserSessionRepository>();
            return userSessionRepository.GetLoggedInUser().FullName;
        }

        public static List<Users> GetUsers(this HtmlHelper helper)
        {
            var dependencyResolver = DependencyResolver.Current as AutofacDependencyResolver;
            var queryUser = dependencyResolver.GetService<IQueryUserService>();
            return queryUser.GetAllUsers().ToList();
        }
       
    }
}