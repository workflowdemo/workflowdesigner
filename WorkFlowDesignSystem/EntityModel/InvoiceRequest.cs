﻿using System;
using System.Data.Linq.Mapping;
using System.Runtime.Serialization;

namespace Cygrp.Workflow.Web.EntityModel
{
    [Serializable]
    [DataContract]
    [Table(Name="InvoiceRequest")]
    public class InvoiceRequest
    {
        [DataMember]
        [Column(IsPrimaryKey=true)]
        public Guid InvoiceId { get; set; }

        [DataMember, Column]
        public string InvoiceNumber { set; get; }

        [DataMember, Column]
        public decimal Amount { get; set; }

        [DataMember, Column(Name="CreatedDate")]
        public DateTime CreationDate { get; set; }

        [DataMember]
        public DateTime UpdationDate { get; set; }

        [DataMember, Column]
        public DateTime DueDate { get; set; }

        [DataMember, Column]
        public string ReferenceClientId { get; set; }

        [DataMember, Column]
        public string ReferenceFileId { get; set; }

        [DataMember, Column]
        public string CreatedBy { get; set; }

        [DataMember, Column]
        public string UpdatedBy { get; set; }

        [DataMember, Column]
        public string Description { get; set; }

        [DataMember, Column]
        public int AssignedUserId { get; set; }

        [DataMember, Column]
        public int CreatedByUserId { get; set; }

        [DataMember, Column]
        public Guid WorkflowInstanceId { get; set; }

        [DataMember]
        public int ReferenceProcessType { get; set; }

        [DataMember, Column]
        public bool IsDeliveredToClient { get; set; }

        [DataMember, Column]
        public bool IsSubmittedToAccountIn { get; set; }

        [DataMember]
        public bool IsCompletedFromAccounts { get; set; }

        [DataMember]
        public bool IsFollowUpCompleted { get; set; }

        [DataMember, Column]
        public bool IsCancelled { get; set; }

        [DataMember, Column]
        public bool IsCompleted { get; set; }

        [DataMember]
        public TimeSpan ClientFollowupTime { get; set; }

        [DataMember]
        public int ActivityType { get; set; }

        [DataMember]
        public int EscalationDays { get; set; }

        [DataMember]
        public string Comments { get; set; }

        [DataMember]
        public int Indicator { get; set; }

    }
}
