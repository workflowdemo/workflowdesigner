﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Web.EntityModel
{
   [Table(Name = "CourtAppointment")]
    public class CourtAppointment
    {

        [Column(IsPrimaryKey = true)]
        public Guid Id { get; set; }
        [Column]
        public DateTime CourtAppointmentDate { get; set; }
        [Column]
        public DateTime PreparationDate { get; set; }
        [Column]
        public bool? IsDocumentPrepared { get; set; }
        [Column]
        public bool? IsSendReminder { get; set; }
        [Column]
        public DateTime? ReportToClientDate { get; set; }
        [Column]
        public bool? IsReportToClient { get; set; }
        [Column]
        public DateTime CreatedOn { get; set; }
        [Column]
        public long CreatedBy { get; set; }
        [Column]
        public long AssignedTo { get; set; }
    }
}