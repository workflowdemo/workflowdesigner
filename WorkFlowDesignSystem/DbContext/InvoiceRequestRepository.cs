﻿using Cygrp.Workflow.Web.EntityModel;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Cygrp.Workflow.Web.DbContext
{
    public static class InvoiceRequestRepository
    {
        static string connectionString = ConfigurationManager.ConnectionStrings["InvoiceWorkFlow"].ConnectionString;
        public static void SaveInvoiceRequest(InvoiceRequest invoiceRequest)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "SaveInvoiceRequest";

                command.Parameters.Add(new SqlParameter("@invoiceId", invoiceRequest.InvoiceId));
                command.Parameters.Add(new SqlParameter("@invoiceNumber", invoiceRequest.InvoiceNumber));
                command.Parameters.Add(new SqlParameter("@amount", invoiceRequest.Amount));
                command.Parameters.Add(new SqlParameter("@creationDate", invoiceRequest.CreationDate));
                command.Parameters.Add(new SqlParameter("@dueDate", invoiceRequest.DueDate));
                command.Parameters.Add(new SqlParameter("@referenceClientId", invoiceRequest.ReferenceClientId));
                command.Parameters.Add(new SqlParameter("@referenceFileId", invoiceRequest.ReferenceFileId));
                command.Parameters.Add(new SqlParameter("@createdBy", invoiceRequest.CreatedBy));
                command.Parameters.Add(new SqlParameter("@description", invoiceRequest.Description));
                command.Parameters.Add(new SqlParameter("@assignedUserId", invoiceRequest.AssignedUserId));
                command.Parameters.Add(new SqlParameter("@createdByUserId", invoiceRequest.CreatedByUserId));
                command.Parameters.Add(new SqlParameter("@workflowInstanceId", invoiceRequest.WorkflowInstanceId));
                command.Parameters.Add(new SqlParameter("@isCompleted", invoiceRequest.IsCompleted ? 1 : 0));
                command.Parameters.Add(new SqlParameter("@isDeliveredToClient", invoiceRequest.IsDeliveredToClient ? 1 : 0));
                command.Parameters.Add(new SqlParameter("@isSubmittedToAccountIn", 1));
                command.Parameters.Add(new SqlParameter("@isCancelled", invoiceRequest.IsCancelled ? 1 : 0));

                command.ExecuteNonQuery();
            }
        }

        public static DataTable GetInvoiceTrackingDetails(string invoiceID)
        {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            //SqlCommand command = con.CreateCommand();
            SqlCommand command = new SqlCommand("GetInvoiceTrackingDetails", con);
            command.CommandType = CommandType.StoredProcedure;
            //command.CommandText = "GetInvoiceTrackingDetails";
            command.Parameters.Add(new SqlParameter("@InvoiceID", invoiceID));

            SqlDataAdapter da = new SqlDataAdapter(command);

            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

    }
}
