﻿using Cygrp.Workflow.Web.EntityModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Web.DbContext
{
    public class AppDbContext : DataContext
    {
        public AppDbContext()
            : base(ConfigurationManager.ConnectionStrings["InvoiceWorkFlow"].ConnectionString)
        {

        }
        
        public Table<InvoiceRequest> InvoiceRequest;
        public Table<CourtAppointment> CourtAppointment;
    }
}