﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Core.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Web.Mvc;
using WorkFlowApplication.Repository;
using WorkFlowApplication.Data;
using System.Data.Linq;
using WorkFlowApplication.Workflow;
using Cygrp.Workflow.Infrastructure.Repository;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core;
using WorkFlowApplication.Workflow.Activities;
using Cygrp.Workflow.Infrastructure;

namespace Cygrp.Workflow.Web.App_Start
{
    public class DependencyResolverConfig
    {
        public static void RegisterCustomTypes(ContainerBuilder builder)
        {
            builder.RegisterType<LinqToSqlWorkflowService>().As<IWorkflowService>();

            builder.RegisterType<QueryUserService>().As<IQueryUserService>();


            builder.RegisterType<WorkflowService>().As<DataContext>();

            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepository<>));

            builder.RegisterType<SqlExpressionEvaluator>().As<IExpressionEvaluator>();

            builder.RegisterType<UserSessionRepository>().As<IUserSessionRepository>();

            builder.RegisterType<WorkflowInstanceRepository>().As<IWorkflowInstanceRepository>();


            builder.RegisterType(typeof(WorkflowApplication)).AsSelf();

            builder.RegisterType(typeof(DelayActivity)).AsSelf();

           
        }
    }
}