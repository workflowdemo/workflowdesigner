﻿using Autofac;
using Autofac.Integration.Mvc;
using Cygrp.Workflow.Web.App_Start;
using Hangfire;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(Cygrp.Workflow.Web.Startup))]

namespace Cygrp.Workflow.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

          
		    GlobalConfiguration.Configuration
                .UseSqlServerStorage("InvoiceWorkFlow");

           // app.UseAutofacMiddleware(container);
            //app.use

         
          var builder = new ContainerBuilder();

            // Register your MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            DependencyResolverConfig.RegisterCustomTypes(builder);
            // OPTIONAL: Enable property injection in view pages.
            // builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            builder.RegisterFilterProvider();

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
            GlobalConfiguration.Configuration.UseActivator(new ContainerJobActivator(container));
            //app.UseAutofacMvc();
            app.UseHangfireDashboard();
            app.UseHangfireServer();

        }

    }

    public class ContainerJobActivator : JobActivator
    {
        private IContainer _container;

        public ContainerJobActivator(IContainer container)
        {
            _container = container;
        }

        public override object ActivateJob(Type type)
        {
            return _container.Resolve(type);
        }
    }
}
