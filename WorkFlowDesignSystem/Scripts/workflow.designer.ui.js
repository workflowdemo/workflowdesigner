﻿/// <reference path="workflowjs.js" />

$((function ($, window, wfDesigner, jsPlumb) {

    var $ui,
		$progressBar,
		$droappableArea,
		$createNewProcedureButton,
		$saveNewProcedureButton,
        $activities,
        commonEndPointOptions,
        $procedureSaveConfirmationDialog,
        customAttributes = [];
   // AddcustomAttributeItems = [];
  
    
    function createContextMenu(Clone) {
        $(Clone).on("contextmenu", function (event) {
            var i = 0;
            var currentCloneId = this.id;
            event.preventDefault();

            $(".custom-menu").finish()
                .toggle(100)
                .css({ top: event.pageY + "px", left: event.pageX + "px" });

            $(".custom-menu li").click(function (e) {
                if ($(e.target).text() == 'Delete' && i == 0) {
                    i = 1;
                    jConfirm('Are you sure you want to delete the activity and all its connections?', 'Confirmation Dialog', function (r) {
                        if (r == true) {
                            $(Clone).remove();
                            jsPlumb.repaintEverything();
                            jsPlumb.detachAllConnections(Clone);
                            jsPlumb.empty(Clone);

                            $ui.trigger(ui.events.activityDeleted, currentCloneId);

                            $(".custom-menu").hide();
                        }
                    });

                }
                else if ($(e.target).text() == 'Edit Text' && i == 0) {
                    jPrompt('Please enter the Text:', '', 'Edit Text Prompt Dialog', function (labelName) {
                        //$(Clone).empty();
                        var CloneSpanid = "";
                        if (labelName != null && labelName != '') {
                            if (currentCloneId.indexOf('start') > -1) {
                                $("span", $(Clone)).text(labelName);
                                jsPlumb.repaintEverything();

                            } else if (currentCloneId.indexOf('Decesion') > -1) {

                                $("span", $(Clone)).text(labelName);//.addClass("Daimondspantext");//.addClass("Daimondspantext");
                                //var top = $(Clone).css('top');
                                ////$(Clone).innerWidth($(Clone).css('top'));
                                ////$(Clone).innerHeight($(Clone).css('top'));
                                //console.log($(Clone).css('top'));
                              
                               $(Clone).width(60);
                                $(Clone).height(60);
                                jsPlumb.repaintEverything();
                               
                            } else if (currentCloneId.indexOf('Process') > -1) {
                                $("span", $(Clone)).text(labelName);
                                jsPlumb.repaintEverything();
                            }
                            else if (currentCloneId.indexOf('stop') > -1) {
                                $("span", $(Clone)).text(labelName);
                                jsPlumb.repaintEverything();

                            }
                            else {
                                alert("Enter the valid text");
                                return false;
                            }
                            i = 1;

                            $ui.trigger(ui.events.activityNameChanged, { cloneId: currentCloneId, text: labelName });

                        }
                        $(".custom-menu").hide();
                    });
                }
                else if ($(e.target).text() == 'Properties' && i == 0) {
                    $ui.trigger(ui.events.initialiseAttributeRendering, { activityId: currentCloneId });
                    return false;
                }
            });
        });
    }

    $("#prop-modal-delay").on("click", function () {
         if ($(this).is(':checked')) {
             $("#txtdelay").show();
             $('#btnSaveAttributes').show();
        }
        else {
             $("#txtdelay").hide();
             activity.Behaviour = null;
             $('#btnSaveAttributes').hide();
        }
       });
    
   
    $(document).on('click', 'button.removebutton', function () {
        $(this).closest('tr').remove();
        var rowCount = $('#tblviewcustomattributes tr').length;
        if (rowCount == 1) {
            $("#tblviewcustomattributes").hide();
            $('#btnSaveAttributes').hide();
        }
       
        return false;
       
    });
       
    $("#btnaddattribute").on("click", function () {
        var i = 0;
        if ($("#attributename").val() != "" && $("#attributevalue").val() != "") {
            $("#tblviewcustomattributes").show();
            $('#tblviewcustomattributes').append('<tr><td>' + $("#attributename").val() + '</td><td>' + $("#attributevalue").val() + '</td><td><button type="button"  class="removebutton" title="Remove this row">X</button></td></tr>');
           
            // var myKeys = $('#ddlKeys');
            var CustomAttributeitem = {};
            CustomAttributeitem["AttributeKey"] = $("#attributename").val();
            CustomAttributeitem["AttributeValue"] = $("#attributevalue").val();
            CustomAttributeitem["AttributeType"] = "property";
            customAttributes.push(CustomAttributeitem);
            $("#attributename").val('');
            $("#attributevalue").val('');
            $('#btnSaveAttributes').show();
        }
        else {
            jAlert("Please Enter the  Attribute Name and Value", "Save Custom Attribute", function (c) {
                return false;
            });
          
        }
    });
    $("#btnAddAttributes").on("click", function () {
        var myKeys = $('#ddlKeys');
        var item = {};
        item["AttributeKey"] = myKeys.val();
        item["AttributeValue"] = $('#txtValue').val();
        Delayitem["AttributeType"] = "route";
        customAttributes.push(item);

        //var shape = workflowProcess.getOrCreateShape({ Id: myKeys.val() });
        $('#tblCustomAttributes').show();
        $('#btnSaveAttributes').show();
        $('#tblCustomAttributes').append('<tr><td>' + $("option:selected",myKeys).text() + '</td><td>' + $('#txtValue').val() + '</td></tr>');

    });

    var ui = {

        events: {
            procedureSave: "wfdesigner.ui.save",
            createNewProcedure: "wfdesigner.ui.createProcedure",
            connectionCreated: "wfdesigner.ui.connectionCreated",
            connectionDetached: "wfdesigner.ui.connectionDetached",
            activityCreated: "wfdesigner.ui.activityDropped",
            activityDeleted: "wfdesigner.ui.activityDeleted",
            activityNameChanged: "wfdesigner.ui.activityNameChanged",
            initialiseAttributeRendering: "wfdesigner.ui.initialiseAttributeRendering"
        },
        initialize: function () {
            $ui = $(this);
            $progressBar = $(".LoadingMessagemodal");
            $droappableArea = $("#DroppableArea");
            $createNewProcedureButton = $("#createNew");
            $saveNewProcedureButton = $("#btnInitiateSave");
            $activities = $(".workflow");
            $procedureSaveConfirmationDialog = $("#dialog-7");
            commonEndPointOptions = {
                endpoint: ["Dot", { radius: 7 }],
                endpointStyle: { fillStyle: "#5b9ada" },
                isSource: true,
                isTarget: true,
                connector: ["Flowchart"],
                maxConnections: 4,
                connectorHoverStyle: { strokeStyle: "black" },
                connectorOverlays: [
                                    ["Arrow", { width: 10, length: 10, foldback: 1, location: 1, id: "arrow" }],
			                        ["Label", { location: 0.9, id: "label", cssClass: "aLabel" }]
                ]
            };
            
            $procedureSaveConfirmationDialog.dialog({ autoOpen: false, title: 'Save Procedure', buttons: {}, width: 390 });

            jsPlumb.setContainer($droappableArea);

            $droappableArea.draggable({
                scroll: true,
                disabled: true
            });

            $activities.draggable({
                appendTo: "body",
                helper: "clone",
                cursor: 'move',
                tolerance: 'fit',
                scroll: true
            });

            $droappableArea.on("scroll", function () {
                jsPlumb.repaintEverything();
            });

            jsPlumb.bind("connection", function (info) {
                $ui.trigger(ui.events.connectionCreated, info);
            });

            jsPlumb.bind("connectionDetached", function (info) {
                $ui.trigger(ui.events.connectionDetached, info);
            });

            $(window.document).bind("mousedown", function (e) {
                // If the clicked element is not the menu
                if (!$(e.target).parents(".custom-menu").length > 0) {

                    // Hide it
                    $(".custom-menu").hide(100);
                }
            });

            $droappableArea.droppable({
                drop: function (event, _ui) {

                    if (_ui.draggable.parent().attr('id') == "DraggableImages") {
                        var newClone = $(_ui.helper).clone();
                        newClone.attr('id', _ui.draggable.attr('id') + Math.floor(Math.random() * 1000));
                        newClone.attr('name', _ui.draggable.attr('name'));
                        ui.appendActivityToDesigner(newClone, _ui.helper.offset());
                       
                    }

                }
            });

            $createNewProcedureButton.click(function () {
                $ui.trigger(ui.events.createNewProcedure);
            });

            $saveNewProcedureButton.click(function () {
                $ui.trigger(ui.events.procedureSave);
            });

            $(window).on("resize", function () {
                jsPlumb.repaintEverything();
            });

        },
        showDroppableArea: function () {
            $droappableArea.show();
        },
        hideDroppableArea: function () {
            $droappableArea.hide();
        },
        showProgressBar: function () {
            $progressBar.show();
        },
        hideProgressBar: function () {
            $progressBar.hide();
        },
        resetDesigner: function () {
            jsPlumb.empty("DroppableArea");
            $droappableArea.show();
            $droappableArea.empty();
            $saveNewProcedureButton.removeClass("hide").addClass("show");
            jsPlumb.repaintEverything();
        },
        openSaveConfirmationDialog: function () {
            $procedureSaveConfirmationDialog.dialog("open");
        },
        closeSaveConfirmationDialog: function () {
            $procedureSaveConfirmationDialog.dialog('close');
        },
        displayAddAttributesUI: function (activity, process) {
            var iWidth = 550; var iHeight = 600;
            $('#dvActivityAttribute .modal-dialog').css("width", iWidth);
            $('#dvActivityAttribute .modal-dialog').css("height", iHeight);
            $('#dvActivityAttribute .modal-title').html("Activity Properties");

            $('#dvActivityAttribute').modal({ show: true });

            $('#tblCustomAttributes').find("tr:gt(0)").remove();
            $('#tblCustomAttributes').hide();
            $("#btnSaveAttributes").unbind("click");
            $('#btnSaveAttributes').hide();

            var myKeys = $('#ddlKeys');
            myKeys.empty();
            $('#txtValue').val('');

            myKeys.on("change", function () {
                $('#txtValue').val('');
            });
            $('#prop-modal-Id').text(activity.Id);
            $('#prop-modal-name').text(activity.Text);
            $('#prop-modal-status').val(activity.Text);
           
          
            


            //var shape = workflowProcess.getOrCreateShape({ Id: currentCloneId });
            //customAttributes = [];

            if (activity.Connections != null) {
                $.each(activity.Connections, function (index, conn) {
                    var target = process.getOrCreateShape({ Id: conn.TargetId }).Text;
                    myKeys.append(
						$('<option></option>').val(conn.TargetId).html(target)
					);
                });
            }

            if (activity.CustomAttributes && (activity.CustomAttributes.length > 0)) {
                $('#tblCustomAttributes').show();
                //View Execution Type
                if (activity.ExecutingUserType = 'U')
                {
                    $('#IsUserActivity').attr('checked', true);
                }
                else {
                    $('#IsUserActivity').attr('checked', true);
                }
                //End

                var isdelay = false;
               // console.log("Behaviour : " + activity.Behaviour + "" + activity.CustomAttributes.AttributeValue);
                if (activity.Behaviour == "delay") {
                    isdelay = true;
                   $('#txtdelay').show();
                    $('#prop-modal-delay').attr('checked', true);
                }

                $.each(activity.CustomAttributes, function (index, attr) {
                    if (attr.AttributeType == "route") {
                        $('#tblCustomAttributes').append('<tr><td>' + $("#ddlKeys option[value='" + attr.AttributeKey + "']").text().trim() + '</td><td>' + attr.AttributeValue + '</td></tr>');
                    }
                    else if (attr.AttributeType == "property" && attr.AttributeKey!= "@@DelayInSeconds") {
                        $('#tblviewcustomattributes').show();
                        $('#tblviewcustomattributes').append('<tr><td>' + attr.AttributeKey + '</td><td>' + attr.AttributeValue + '</td><td><button type="button"  class="removebutton" title="Remove this row">X</button></td></tr>');
                    }
                    else if(isdelay==true){
                        $('#txtdelay').show();
                        $('#txtdelay').val(attr.AttributeValue);
                        $('#prop-modal-delay').attr('checked', true);
                      }
                });
            }

            customAttributes = [];

            if ($('#ddlKeys option').length != 0) {
                $("#btnSaveAttributes").on("click", function () {
                 
                    if ($("#txtdelay").val() != "" && $("#txtdelay").val() != null) {
                        var Delayitem = {};
                        Delayitem["AttributeKey"] = "@@DelayInSeconds";
                        Delayitem["AttributeValue"] = $('#txtdelay').val();
                        Delayitem["AttributeType"] = "property";
                        customAttributes.push(Delayitem);
                        activity.Behaviour = "delay";
                    }
                    activity["CustomAttributes"] = customAttributes;
                    // activity["AddcustomAttributeItems"] = AddcustomAttributeItems;
                    if ($('#IsUserActivity').attr('checked')) {
                        activity.ExecutingUserType='U';
                    } else {
                        activity.ExecutingUserType = 'S';
                    }
                   // console.log("customAttributes"+customAttributes)
                    
                });
            }
        },
        appendActivityToDesigner: function (newClone, newOffset) {
            
            var sam = newClone.appendTo($droappableArea);
            if (newOffset) {
                sam.offset(newOffset);
            }
          
                jsPlumb.addEndpoint(newClone, {
                    anchors: ["TopCenter"]
                }, commonEndPointOptions);

                jsPlumb.addEndpoint(newClone, {
                    anchors: ["RightMiddle"]
                }, commonEndPointOptions);

                jsPlumb.addEndpoint(newClone, {
                    anchors: ["BottomCenter"]
                }, commonEndPointOptions);

                jsPlumb.addEndpoint(newClone, {
                    anchors: ["LeftMiddle"]
                }, commonEndPointOptions);
          
            jsPlumb.draggable(newClone);
           
            $(newClone).resizable({
                containment: $droappableArea,
                handles: "se",
                aspectRatio: true,
                resize: function (event, ui) {
                    //var size = ui.size;
                    //var fontsize = parseInt((size.width * size.height) / 1000);
                    //$(this).css("font-size", parseInt(fontsize) + 11 + "px");
                    $(newClone).css("line-height", $(newClone).outerHeight() + "px");
                   jsPlumb.repaint(ui.helper);
                    jsPlumb.repaintEverything();
                }
                

            });
            createContextMenu(newClone);

            jsPlumb.repaintEverything();
          
            $ui.trigger(ui.events.activityCreated, newClone);
        },

        createActivity: function (param) {
            var activity;
            switch (param.activityType) {
                case "startpoint":
                    activity = $('<div class="create-state" ><span class="Activityspantext">' + param.text + '</span></div>');
                    break;
                case "decision":
                    activity = $('<div class="create-state_daimond" ><span class="spantext Daimondspantext">' + param.text + '</span></div>');
                    break;
                case "task":
                    activity = $('<div class="create-state_rectangle" ><span class="Activityspantext">' + param.text + '</span></div>');
                    break;
                case "endpoint":
                    activity = $('<div class="create-state-End" ><span class="Activityspantext">' + param.text + '</span></div>');
                    break;
            }
            return activity.attr("data-nodetype", param.activityType)
                .addClass("workflow");
        }
    };

	if (!window.wfDesigner) {
		window.wfDesigner = {};
	}

	window.wfDesigner.ui = ui;

}(window.jQuery, window, window.wfDesigner, window.jsPlumb)));