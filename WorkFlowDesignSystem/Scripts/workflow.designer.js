﻿/// <reference path="workflow.designer.ui.js" />

$((function ($, window, jsPlumb, ui) {
    // $(".LoadingMessagemodal").show();

    var wfDesigner, connectionObj,
     commonEndPointOptions = {
         endpoint: ["Dot", { radius: 7 }],
         endpointStyle: {
             fillStyle: "#5b9ada"
         },
         isSource: true,
         isTarget: true,
         connector: ["Flowchart"],


         maxConnections: 4,
         connectorHoverStyle: { strokeStyle: "black" },

         connectorOverlays: [
        ["Arrow", {
            width: 10,
            length: 10,
            foldback: 1,
            location: 1,
            id: "arrow"
        }],
        ["Label", {
            location: 0.9,
            id: "label",
            cssClass: "aLabel"
        }]
         ]
     },
     workflowProcess = {
         WorkflowProcessID: "",
         Name: "",
         Description: "",
         Shapes: [],
         load: function (process) {
             this.reset();
             if (process != null && process != undefined) {
                 this.WorkflowProcessID = process.WorkflowProcessID
                 this.Name = process.Name;
                 this.Description = process.Description;
                 this.Shapes = process.Shapes;
             }
         },
         reset: function () {
             this.Description = "";
             this.Shapes = [];
             this.Name = "";
             this.WorkflowProcessID = "";
         },
         getOrCreateShape: function (_shape) {
             var _res = _.find(this.Shapes, function (shape) {
                 return shape.Id == _shape.Id;
             });

             if (_res == undefined) {
                 _res = _shape;
                 this.Shapes.push(_res);
             }

             return _res;
         },
         getOrCreateConnection: function (sId, tId) {
             var shape = this.getOrCreateShape({ Id: sId });
             shape.Connections = shape.Connections || [];
             var conn = _.findWhere(shape.Connections, { TargetId: tId });
             if (!conn || conn == null || conn == undefined) {
                 conn = { TargetId: tId };
                 shape.Connections.push(conn);
             }
             return conn;
         },
         removeConnection: function (sId, tId) {
             var shape = this.getOrCreateShape({ Id: sId });
             shape.Connections = _.reject(shape.Connections, function (d) { return d.TargetId === tId; });
         },
         removeShape: function (sId) {
             this.Shapes = _.reject(this.Shapes, function (d) { return d.Id === sId; });
         }
     };

    $('#btnCancel').click(function () {
        ui.closeSaveConfirmationDialog();
    });
  
    $(".workflow-link").live("click", function () {
        ui.showProgressBar();
        jsPlumb.empty("DroppableArea");
        var workflowName = $(this).attr("data-workflowId");
        $.ajax({
            url: '/Home/NewProcedure/',
            type: "GET",
            contentType: 'html',
            success: function (data) {
                AddViewToContainer(data);
                $("#dragContainer").removeClass("hide");
                ShowPalleteMenuItem();
                $.ajax({
                    url: '/Home/GetProcess/',
                    type: "POST",
                    dataType: 'json',
                    data: { name: workflowName }
                })
                .done(function (data) {
                    if (data != null && data != "") {

                        $("#DroppableArea").show();
                        $("#DroppableArea").empty();
                        workflowProcess.reset();
                        workflowProcess.load(data);
                        loadFlowchart(data);
                        $("#btnInitiateSave").removeClass("hide").addClass("show");
                        $("#displayWorkflowName").removeClass("glyphicon glyphicon-plus-sign")
                        $("#displayWorkflowName").text(workflowName);

                        $(".workflowInstanceActionListItem").remove();
                        $("#dividerActionListItem").remove();

                        $("#workflowActionList").prepend('<li id="dividerActionListItem" class="divider"></li>')
                        $("#workflowActionList").prepend('<li class="workflowInstanceActionListItem" data-instance_Url="' + data.Description + '"><a href="#" >Create ' + workflowName + '</a></li>');
                        
                        jsPlumb.repaintEverything();
                    }

                    else {
                        jAlert('Error on loading selected procedure', 'Error Dialog');
                    }

                }).fail(function (jqxhr) {

                    jAlert('Error on loading selected procedure', 'Error Dialog');
                    console.log("error while loading selected procedure Error");
                    console.log(jqxhr);
                    return false;
                }).always(function () {
                    ui.hideProgressBar();
                    $("#txtfilename").val('');
                });
            }
        });
    });

    $('#btnload').click(function () {
        //loadFlowchart();
        return false;

    });
    $('#btnSave').click(function () {

        SetConnectionText();
    });
    $('#btnSaveProcess').click(function () {
        var processName = $("#txtfilename").val();
        if (processName != '' && processName != null) {
            workflowProcess.Name = processName;

            UpdatedCloneAttributeValues(workflowProcess);
           saveWorkflow();
        }
        else {
            jAlert("Please enter the valid procedure name.", "Error");
            return false;
        }
    });
    $('#btnshapeSave').click(function () {

        var lable = $("#txtshapeLabel").val();
        newid.append(lable);

    });

    $('#viewInvoices').click(function () {
        ui.showProgressBar();
        jsPlumb.empty("DroppableArea");
        $.ajax({
            url: '/Invoice/ViewInvoices/',
            type: 'GET',
            contentType: 'html',
            success: function (data) {
                ui.hideProgressBar();
                $("#dragContainer").addClass("hide");
                HidePalleteMenuItem();
                AddViewToContainer(data);
               
            },            
            error: function (ex) {
                jAlert('Error', 'error');
                ui.hideProgressBar();
            }
        });

    });

    $("#workflowActionList").on('click', '.workflowInstanceActionListItem', (function (e) {
        ui.showProgressBar();

        var instance_Url = $(this).attr('data-instance_Url');

        if (!instance_Url || instance_Url ==='null') {
            jAlert("Instance not available");
            ui.hideProgressBar();
            return;
        }

        jsPlumb.empty("DroppableArea");
        $.ajax({
            url: instance_Url,
            type: 'GET',
            contentType: 'html',
            success: function (data) {
                ui.hideProgressBar();
                AddViewToContainer(data);
                $("#dragContainer").addClass("hide");
                HidePalleteMenuItem();
            },
            error: function (ex) {
                jAlert('Error', 'error');
                ui.hideProgressBar();
            }
        });

    }));

    $('#createNew').click(function () {
        ui.showProgressBar();
        jsPlumb.empty("DroppableArea");
        $("#DroppableArea").html('');
        $.ajax({
            url: '/Home/NewProcedure/',
            type: 'GET',
            contentType: 'html',
            success: function (data) {
                ui.hideProgressBar();
                $('#dvContainer').html('');
                $('#dvContainer').html(data);
                $("#dragContainer").removeClass("hide");
                ShowPalleteMenuItem();
                $("#displayWorkflowName").addClass("glyphicon glyphicon-plus-sign")
                $("#displayWorkflowName").html(' Create');
                $("#dividerActionListItem").remove();
                $(".workflowInstanceActionListItem").remove();
            },
            error: function (ex) {
                jAlert('Error', 'error');
                ui.hideProgressBar();
            }
        });
    });
    
    $('#dashboard').click(function () {
        ui.showProgressBar();
        $.ajax({
            url: '/Dashboard/Index/',
            type: 'GET',
            contentType: 'html',
            success: function (data) {
                ui.hideProgressBar();
                AddViewToContainer(data);
                $("#dragContainer").addClass("hide");
                HidePalleteMenuItem();
                jsPlumb.empty("DroppableArea");
            },
            error: function (ex) {
                jAlert('Error while loading dashboard', 'Error');
                ui.hideProgressBar();
            }
        });

    });

    function HidePalleteMenuItem() {
        //$("#imagePalleteSideMenuItem").addClass("hide");
        $("#imagePalleteSideMenuItem").hide(400);
    }

    function ShowPalleteMenuItem() {
        //$("#imagePalleteSideMenuItem").removeClass("hide");
        $("#imagePalleteSideMenuItem").show(400);
    }

    function AddViewToContainer(data)
    {
        jsPlumb.repaintEverything();
        $('#dvContainer').html('');
        $('#dvContainer').html(data);
    }

    jsPlumb.bind("contextmenu", function (connection, e) {
        e.preventDefault();
        GetEdgeContextMenu(connection);
    });

    function GetEdgeContextMenu(connection) {
        //connectionObj = connection;
        //$('#txtLabel').val(connectionObj.getLabel());
        var i = 0;
        $(".custom-menu").finish().toggle(100).
       css({
           top: event.pageY + "px",
           left: event.pageX + "px"
       });
        $(".custom-menu li").click(function (e) {
            if ($(e.target).text() == 'Delete' && i == 0) {
                i = 1;
                jConfirm('Are you sure you want to delete Edge?', 'Confirmation Dialog', function (r) {
                    if (r == true) {
                        jsPlumb.detach(connection);
                        $(".custom-menu").hide();
                    }
                });

            }
            else if ($(e.target).text() == 'Edit Text' && i == 0) {
                jPrompt('Please enter the Text:', '', 'Edit Text Prompt Dialog', function (labelName) {
                    if (labelName != null && labelName != '') {
                        SetConnectionText(labelName, connection);
                    }
                });
            }
            else { }
        });

    }

    function showModal(connection) {
        connectionObj = connection;
        $('#txtLabel').val(connectionObj.getLabel());
        var iWidth = 500; var iHeight = 400;
        $('#dvTemplateJT .modal-dialog').css("width", iWidth);
        $('#dvTemplateJT .modal-dialog').css("height", iHeight);
        $('#dvTemplateJT .modal-title').html("Add text for the connection");
        $('#dvTemplateJT').modal({ show: true });
        return false;
    }

    function saveWorkflow() {
       
        var jsonData = { WorkflowProcessID: workflowProcess.WorkflowProcessID,  Name: workflowProcess.Name, Description: workflowProcess.Description, Shapes: workflowProcess.Shapes };
        //console.log(jsonData);
        $.ajax({
            url: "/Home/SaveWorkflow",
            type: "POST",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(jsonData),
            success: function (data) {
                console.log(data);
                jAlert("Workflow with Name '" + workflowProcess.Name + "' has been saved.", "Save Procedure", function (c) {
                    if (c == true) {
                        ui.closeSaveConfirmationDialog()
                        $("#DroppableArea").empty();
                        $("#txtfilename").val('');
                        jsPlumb.repaintEverything();
                        workflowProcess.reset();
                        location.reload();
                    }
                });
            },

            error: function () {
                jAlert('Error On Creating File Name', 'Error');
                return false;
            }
        });
    }

    function loadFlowchart(data) {
       
        $.each(data.Shapes, function (index, elem) {
            var position = "position: absolute; left:" + elem.positionX + "px; top:" + elem.positionY + "px;width:" + elem.Width + "px;height:" + elem.Height + "px;";
            var activity = ui.createActivity({ activityType: elem.TypeText, text: elem.Text })
             .attr("id", elem.Id)
             .attr("style", position);

            ui.appendActivityToDesigner(activity);
        });

        $.each(data.Shapes, function (index, elem) {
            if (!elem.Connections || elem.Connections === null || elem.Connections === undefined || !elem.Connections.length) {
                console.log("skipping connection less shape:{" + elem.Text + "}");
                return;
            }

            $.each(elem.Connections, function (index, conn) {
                
               var connection = jsPlumb.connect({
                    source: elem.Id,
                    target: conn.TargetId,
                    label: conn.Text,
                    anchors: [conn.SourceAnchorType,conn.TargetAnchorType],
                    repaint: true,
                    overlays: [["Arrow", {
                        width: 10,
                        length: 10,
                        foldback: 1,
                        location: 1,
                        id: "arrow"
                    }]]
                }, commonEndPointOptions);

              // console.log(connection);
            });
        });

    }

    function SetConnectionText(labelName, connectionObj) {
        //console.log(connectionObj);
        connectionObj.setLabel({
            label: labelName,
            location: 0.5,
            id: 'label',
            cssClass: 'pointer-label'
        });
        workflowProcess.getOrCreateConnection(connectionObj.sourceId, connectionObj.targetId).Text = labelName;
    }
    function UpdatedCloneAttributeValues(workflowProcess) {
        $.each(workflowProcess.Shapes, function (index, Act) {
            Act.Width = parseInt($("#" + Act.Id).css("width"));
            Act.Height = parseInt($("#" + Act.Id).css("height"));
            Act.positionX = parseInt($("#" + Act.Id).css("left"), 10);
            Act.positionY = parseInt($("#" + Act.Id).css("top"), 10);
           
        });
    }
    $(ui).bind(ui.events.procedureSave, function () {

        if (workflowProcess && workflowProcess !== undefined && (workflowProcess.Name !== "")) {
            UpdatedCloneAttributeValues(workflowProcess);

            saveWorkflow();
            return false;
        }

        ui.openSaveConfirmationDialog();

    });

    $(ui).bind(ui.events.createNewProcedure, function () {

        if (workflowProcess && workflowProcess !== undefined && (workflowProcess.Shapes.length > 0)) {
            jConfirm('Workflow in progress, do you want to save your changes?', 'Confirmation Dialog', function (confirmSave) {
                if (confirmSave == true) {
                    if (workflowProcess.Name === "") {
                        ui.openSaveConfirmationDialog();
                    } else {
                        saveWorkflow();
                    }
                }

            });
            ui.resetDesigner();
            workflowProcess.reset();
           
        }
       

    });

    $(ui).bind(ui.events.connectionCreated, function (e, info) {
        console.log(info);
        workflowProcess.getOrCreateConnection(info.sourceId, info.targetId);
        var conn = workflowProcess.getOrCreateConnection(info.sourceId, info.targetId);
        conn.SourceAnchorType = info.sourceEndpoint.anchor.anchors[0].type;
        conn.TargetAnchorType = info.targetEndpoint.anchor.anchors[0].type;
    });

    $(ui).bind(ui.events.connectionDetached, function (e, info) {
        workflowProcess.removeConnection(info.sourceId, info.targetId);
    });

    $(ui).bind(ui.events.activityCreated, function (e, activity) {
        var newClone = $(activity);
        workflowProcess.getOrCreateShape({
            Id: newClone.attr('id'),
            Text: newClone.text(),
            TypeText: newClone.attr('data-nodetype'),
            positionX: parseInt(newClone.css("left"), 10),
            positionY: parseInt(newClone.css("top"), 10),
            Width: parseInt(newClone.css("width"), 10),
            Height: parseInt(newClone.css("height"), 10)
        });
    });

    $(ui).bind(ui.events.activityDeleted, function (e, currentCloneId) {
        workflowProcess.removeShape(currentCloneId);
    });

    $(ui).bind(ui.events.activityNameChanged, function (e, obj) {
        workflowProcess.getOrCreateShape({ Id: obj.cloneId }).Text = obj.text;
    });

    $(ui).bind(ui.events.initialiseAttributeRendering, function (e, activity) {
        var shape = workflowProcess.getOrCreateShape({ Id: activity.activityId });
        ui.displayAddAttributesUI(shape, workflowProcess);
    });
    
    $(function () {

        wfDesigner = window.wfDesigner || {};

        ui.initialize();

        function BindFileList() {
            $.ajax({
                url: '/Home/BindFileNamesToList/',
                type: "Get",
                dataType: 'json'
            }).done(function (data) {
                $("#my-workflow ul").empty();
                $(data).each(function (index, item) { // GETTING ERROR HERE
                    $("#my-workflow ul").append("<li><a class='workflow-link' data-workflowid='" + item.Name + "' href='#'>" + item.Name + "</a></li>");
                });
            }).fail(function () {
                jAlert('Error On Bind File Name', 'Error Dialog');
                return false;
            });
        }

        BindFileList();
    });

})(window.jQuery, window, window.jsPlumb, window.wfDesigner.ui));