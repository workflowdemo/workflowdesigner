USE [WorkflowService]
GO
/****** Object:  StoredProcedure [dbo].[SaveActivityStatus]    Script Date: 12/15/2014 16:59:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveActivityStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveActivityStatus]
GO
/****** Object:  StoredProcedure [dbo].[CompleteInvoiceRequest]    Script Date: 12/15/2014 16:59:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompleteInvoiceRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CompleteInvoiceRequest]
GO
/****** Object:  StoredProcedure [dbo].[CompleteInvoiceRequest]    Script Date: 12/15/2014 16:59:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompleteInvoiceRequest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CompleteInvoiceRequest]
(		
		@invoiceId varchar(400), 
		@completed bit, 
		@actionDoneByUserId int	
)
AS
BEGIN
	UPDATE [WorkflowService].[dbo].[InvoiceRequest] 
	   SET 
	   IsCompleted = @completed, 
	   UpdatedBy = @actionDoneByUserId
 WHERE InvoiceId = @invoiceId
END 
' 
END
GO
/****** Object:  StoredProcedure [dbo].[SaveActivityStatus]    Script Date: 12/15/2014 16:59:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveActivityStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[SaveActivityStatus]
	(	
	
		@workFlowInstanceId varchar(400),
		@activityName varchar(400), 
		@activityType smallint, 
		@actionTaken smallint,
		@referenceFileId varchar(50),
		@referenceClientId varchar(50),
		@createdDate datetime,
		@createdByUserId int,	
		@dueDate datetime,	
		@assignedDate datetime,
		@assignedByUserId int,
		@assignedToUserId int, 
		@isFollowUpRequired bit,
		@isSuccess	bit,
		@isCompleted bit,
		@isCancelled bit,
		@referenceProcessType smallint,
		@referenceProcessID varchar(50),
		@escalationDays int,
		@comments varchar(500)
	)	
AS

BEGIN
	/* SET NOCOUNT ON */ 
declare @teamid int
declare @deptid int

select u.UserID, t.TeamID , d.DepartmentID  into #temp1 from
Users u INNER JOIN Teams t on u.TeamID = T.TeamID
INNER JOIN Departments d ON  T.DeptID  = d.DepartmentID 
where u.UserID = @assignedToUserId

set @teamid = (select TeamID from #temp1)
set @deptid = (select DepartmentID from #temp1)

INSERT INTO InvoiceActivityTracking
		(
		 WorkflowInstanceID,
		 ActivityType,
		 ActivityName, 
		 ActionTaken,   
		 CreatedUser, 
		 DueDate, 
		 AssignedDate, 
		 AssignedByUser,
		 AssignedUser,
		 AssignedTeam,
		 AssignedDepartment,
		 IsCompleted,
		 IsFollowUpRequired,
		 IsCancelled,
		 IsSuccess,
		 ReferenceFileId, 
		 ReferenceClientID,
		 CreatedDate,
		 ReferenceProcessType,
		 ReferenceProcessID,
		 EscalationDays,
		 Comments
		 )
		VALUES
		(
		@workFlowInstanceId,
		@activityType, 
		@activityName, 
		@actionTaken, 
		@createdByUserId, 
		@dueDate,
		@assignedDate, 
		@assignedByUserId,
		@assignedToUserId,
		@teamid,
		@deptid,
		@isCompleted,
		@isFollowUpRequired,
		@isCancelled,
		@isSuccess,
		@referenceFileId,
		@referenceClientId,
		@createdDate, 
		@referenceProcessType,
		@referenceProcessID,
		@escalationDays,
		@comments
		)
		
END
' 
END
GO
