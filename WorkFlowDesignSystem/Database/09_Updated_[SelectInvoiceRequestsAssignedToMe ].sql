USE [WorkflowService]
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsAssignedToMe]    Script Date: 12/15/2014 21:01:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsAssignedToMe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SelectInvoiceRequestsAssignedToMe]
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsAssignedToMe]    Script Date: 12/15/2014 21:01:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsAssignedToMe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SelectInvoiceRequestsAssignedToMe]
	(
		@userId int
	)	
AS
BEGIN
declare @Today date
set @Today =   CAST(CURRENT_TIMESTAMP AS DATE)  
SELECT ir.InvoiceNumber, ir.InvoiceId,ir.Amount,ir.ReferenceClientId,ir.description,
	ir.createdByUserId,ir.IsSubmittedToAccountIn,ir.IsDeliveredToClient,iat.AssignedUser, 
	case when iat.ActivityType = 2 then 1 else 0 end as  IsCompletedFromAccounts,
	iat.ActivityType, iat.duedate, iat.EscalationDays,
	case
		when @Today <= CAST(iat.duedate as date)  then 1
		when @Today > CAST(iat.DueDate AS DATE) AND @Today <= CAST((IAT.DueDate + IAT.EscalationDays) AS DATE) THEN 2
		when @Today > CAST((iat.DueDate + iat.EscalationDays) AS DATE) then 3 
	end Indicator
	FROM InvoiceRequest ir inner join 
	dbo.InvoiceActivityTracking iat on ir.workflowInstanceId = iat.workflowInstanceId 
	inner join (
	select max(workflowTrackingId) workflowTrackingId, workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	from dbo.InvoiceActivityTracking group by workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	) track on iat.workflowTrackingId  = track.workflowTrackingId
	Where iat.AssignedUser = @userId and ir.IsCompleted = 0 
	order by ir.createdDate
	
END
' 
END
GO
