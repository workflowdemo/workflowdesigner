USE [WorkflowService]
GO
/****** Object:  StoredProcedure [dbo].[GetInvoiceTrackingDetails]    Script Date: 12/16/2014 18:37:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetInvoiceTrackingDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetInvoiceTrackingDetails]
GO
/****** Object:  StoredProcedure [dbo].[GetInvoiceTrackingDetails]    Script Date: 12/16/2014 18:37:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetInvoiceTrackingDetails]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetInvoiceTrackingDetails]
@InvoiceID nvarchar(50)
AS
BEGIN
SELECT it.workflowinstanceid, it.ActivityName, it.EventDate, it.Sequence, it.[State],
CASE
	WHEN it.tracktype = 1 THEN ''WorkflowInstanceRecord''
	WHEN it.tracktype = 2 THEN ''ActivityStateRecord''
	WHEN it.tracktype = 3 THEN ''CustomTrackingRecord''
	WHEN it.tracktype = 4 THEN ''BookmarkResumptionRecord''
	WHEN it.tracktype = 5 THEN ''ActivityScheduledRecord''
	WHEN it.tracktype = 6 THEN ''CancelRequestedRecord''
	WHEN it.tracktype = 7 THEN ''FaultPropagationRecord''
	WHEN it.tracktype = 8 THEN ''WorkflowInstanceAbortedRecord''
END as TrackType
FROM WorkflowServiceInstanceTracking it
INNER JOIN invoicerequest ir on it.workflowinstanceid = ir.workflowinstanceid
where ir.invoiceid = @InvoiceID
order by it.sequence
END' 
END
GO
