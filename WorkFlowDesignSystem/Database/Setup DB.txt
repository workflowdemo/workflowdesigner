
/*************** how to setup DB for this Demo App ***************/

1. Make sure you Sql server services are running.
2. Execute all scripts from Database folder in same order as given below (WF instance store schema is created in 
same database).
	
	00_DatabaseCreation.sql
	01_InvoiceWorkFlowScript.sql
	02_SqlWorkflowInstanceStoreLogic.sql
	03_SqlWorkflowInstanceStoreSchema.sql
	04_InvoiceActivityTracking.sql
	05_Reposiroty_Scripts.sql
	06_InvoiceDeliveredToClient.sql
	07_InvoiceRequestsAssignedToMe.sql
	08_Updated[SaveActivityStatus_and_CompleteInvoiceRequest].sql
	09_Updated_[SelectInvoiceRequestsAssignedToMe ].sql
	010_SaveInvoiceWorkflowInstanceTracking.sql
	011_GetInvoiceTrackingDetails.sql
	012_Updated_Reschedule_ReAssign.sql
	013_EnableWorkflowActivity.sql

Note:
- Update DB server in connectionString (with your DB server name) in
	 web.config of InvoiceWeb project
	 App.config file of Invoice.WFServiceHost project.
	web.config of Invoice.Web.WFServiceHost project

- ClearDatabase.sql script can be used to clear existing database (drop db objects and delete data). Make sure to execute step 2 after executing this script.