USE [WorkflowService]
GO
/****** Object:  StoredProcedure [dbo].[ReAssignInvoiceRequest]    Script Date: 12/16/2014 20:27:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReAssignInvoiceRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReAssignInvoiceRequest]
GO
/****** Object:  StoredProcedure [dbo].[ReScheduleInvoiceRequest]    Script Date: 12/16/2014 20:27:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReScheduleInvoiceRequest]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReScheduleInvoiceRequest]
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsAssignedToMe]    Script Date: 12/16/2014 20:27:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsAssignedToMe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SelectInvoiceRequestsAssignedToMe]
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsStartedByMe]    Script Date: 12/16/2014 20:27:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsStartedByMe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SelectInvoiceRequestsStartedByMe]
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsStartedByMe]    Script Date: 12/16/2014 20:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsStartedByMe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SelectInvoiceRequestsStartedByMe]
	(
		@userId int
	)	
AS

BEGIN
	
	
  --  SELECT * 
	 --	FROM InvoiceRequest
		--WHERE CreatedByUserId = @userId		  
		--ORDER BY CreatedDate
		declare @Today date
set @Today =   CAST(CURRENT_TIMESTAMP AS DATE)  
		SELECT ir.InvoiceNumber, ir.InvoiceId,ir.Amount,ir.ReferenceClientId,ir.description,
	ir.createdByUserId,ir.IsSubmittedToAccountIn,ir.IsDeliveredToClient,iat.AssignedUser, 
	ir.IsCompleted, ir.IsCancelled,
	case when iat.ActivityType = 2 then 1 else 0 end as  IsCompletedFromAccounts,
	iat.ActivityType, iat.duedate, iat.EscalationDays,
	case
		when @Today <= CAST(iat.duedate as date)  then 1
		when @Today > CAST(iat.DueDate AS DATE) AND @Today <= CAST((IAT.DueDate + IAT.EscalationDays) AS DATE) THEN 2
		when @Today > CAST((iat.DueDate + iat.EscalationDays) AS DATE) then 3 
	end Indicator
	FROM InvoiceRequest ir inner join 
	dbo.InvoiceActivityTracking iat on ir.workflowInstanceId = iat.workflowInstanceId 
	inner join (
	select max(workflowTrackingId) workflowTrackingId, workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	from dbo.InvoiceActivityTracking group by workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	) track on iat.workflowTrackingId  = track.workflowTrackingId
	Where ir.CreatedByUserId = @userId
	order by ir.createdDate
			  
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsAssignedToMe]    Script Date: 12/16/2014 20:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SelectInvoiceRequestsAssignedToMe]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SelectInvoiceRequestsAssignedToMe]
	(
		@userId int
	)	
AS
BEGIN
declare @Today date
set @Today =   CAST(CURRENT_TIMESTAMP AS DATE)  
SELECT ir.InvoiceNumber, ir.InvoiceId,ir.Amount,ir.ReferenceClientId,ir.description,
	ir.createdByUserId,ir.IsSubmittedToAccountIn,ir.IsDeliveredToClient,iat.AssignedUser, 
	case when iat.ActivityType = 2 then 1 else 0 end as  IsCompletedFromAccounts,
	iat.ActivityType, iat.duedate, iat.EscalationDays,
	case
		when @Today <= CAST(iat.duedate as date)  then 1
		when @Today > CAST(iat.DueDate AS DATE) AND @Today <= CAST((IAT.DueDate + IAT.EscalationDays) AS DATE) THEN 2
		when @Today > CAST((iat.DueDate + iat.EscalationDays) AS DATE) then 3 
	end Indicator
	FROM InvoiceRequest ir inner join 
	dbo.InvoiceActivityTracking iat on ir.workflowInstanceId = iat.workflowInstanceId 
	inner join (
	select max(workflowTrackingId) workflowTrackingId, workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	from dbo.InvoiceActivityTracking group by workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	) track on iat.workflowTrackingId  = track.workflowTrackingId
	Where iat.AssignedUser = @userId
	order by ir.createdDate
	
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ReScheduleInvoiceRequest]    Script Date: 12/16/2014 20:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReScheduleInvoiceRequest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ReScheduleInvoiceRequest]
(		
		@invoiceId varchar(400), 
		@dueDate datetime,
		@actionDoneByUserId int
)
AS
BEGIN
	UPDATE [WorkflowService].[dbo].[InvoiceRequest] 
	   SET 
	   DueDate = @dueDate,
	   UpdatedBy = @actionDoneByUserId
	    WHERE InvoiceId = @invoiceId
	   Update [WorkflowService].[dbo].[InvoiceActivityTracking]
	   Set
	   DueDate = @dueDate
 WHERE ReferenceProcessID = @invoiceId
END 
' 
END
GO
/****** Object:  StoredProcedure [dbo].[ReAssignInvoiceRequest]    Script Date: 12/16/2014 20:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ReAssignInvoiceRequest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[ReAssignInvoiceRequest]
(		
		@invoiceId varchar(400), 
		@assignedUserId int,
		@actionDoneByUserId int
)
AS
BEGIN
	UPDATE [WorkflowService].[dbo].[InvoiceRequest] 
	   SET 
	   AssignedUserId = @assignedUserId,
	   UpdatedBy = @actionDoneByUserId
 WHERE InvoiceId = @invoiceId
  Update [WorkflowService].[dbo].[InvoiceActivityTracking]
	   Set
	   AssignedUser = @assignedUserId
 WHERE ReferenceProcessID = @invoiceId
END 
' 
END
GO
