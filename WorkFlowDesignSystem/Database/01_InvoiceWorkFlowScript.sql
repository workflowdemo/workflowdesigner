USE [WorkflowService]
GO
/****** Object:  Table [dbo].[Workflows]    Script Date: 12/05/2014 15:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Workflows]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Workflows](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowID] [uniqueidentifier] NOT NULL,
	[WorkflowName] [nvarchar](200) NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Workflows] PRIMARY KEY CLUSTERED 
(
	[WorkflowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 12/05/2014 15:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Departments]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Departments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DepartmentID] [int] NOT NULL,
	[DepartmentName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[DepartmentID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Departments] ON
INSERT [dbo].[Departments] ([Id], [DepartmentID], [DepartmentName]) VALUES (1, 1, N'Finance')
INSERT [dbo].[Departments] ([Id], [DepartmentID], [DepartmentName]) VALUES (5, 2, N'Account')
SET IDENTITY_INSERT [dbo].[Departments] OFF
/****** Object:  Table [dbo].[ActivityStatus]    Script Date: 12/05/2014 15:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WorkflowInstanceID] [varchar](400) NOT NULL,
	[ActivityType] [varchar](400) NULL,
	[ReferenceFileID] [varchar](400) NULL,
	[Description] [varchar](400) NULL,
	[ReferenceClientID] [varchar](400) NULL,
	[ReferenceProcessType] [varchar](400) NULL,
	[ReferenceProcessID] [varchar](400) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedUserId] [int] NULL,
	[DueDate] [datetime] NULL,
	[EscalationDays] [int] NULL,
	[AssignedDepartmentId] [int] NULL,
	[AssignedTeamId] [int] NULL,
	[AssignedToUserId] [int] NULL,
	[AssignedDate] [datetime] NULL,
	[AssignedByUserId] [int] NULL,
	[CompletedUserId] [int] NULL,
	[CompletedDate] [datetime] NULL,
	[IsFollowUpRequired] [bit] NULL,
	[FollowUpType] [nvarchar](200) NULL,
	[FollowUpTimeLimit] [bigint] NULL,
	[FollowUpTriesLimit] [int] NULL,
	[IsCompleted] [bit] NULL,
	[IsCancelled] [bit] NULL,
	[IsSuccess] [bit] NULL,
	[ReferenceId] [int] NULL,
	[ActivityName] [varchar](400) NULL,
	[ActionTaken] [varchar](400) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ActivityStatus]') AND name = N'IX_ActivityStatus')
CREATE NONCLUSTERED INDEX [IX_ActivityStatus] ON [dbo].[ActivityStatus] 
(
	[WorkflowInstanceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[InvoiceRequest]    Script Date: 12/05/2014 15:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceRequest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InvoiceRequest](
	[InvoiceId] [uniqueidentifier] NOT NULL,
	[InvoiceNumber] [nvarchar](200) NOT NULL,
	[Amount] [decimal](18, 4) NOT NULL,
	[ReferenceClientId] [nvarchar](200) NULL,
	[CreatedBy] [nvarchar](200) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](200) NULL,
	[IsDeliveredToClient] [bit] NOT NULL,
	[DueDate] [datetime] NOT NULL,
	[ReferenceFileId] [nvarchar](200) NULL,
	[IsSubmittedToAccountIn] [bit] NOT NULL,
	[AssignedUserId] [int] NOT NULL,
	[CreatedByUserId] [int] NOT NULL,
	[WorkflowInstanceId] [uniqueidentifier] NULL,
	[Description] [nvarchar](400) NULL,
	[IsCancelled] [bit] NOT NULL,
	[IsCompleted] [bit] NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceRequest]') AND name = N'IX_InvoiceRequest')
CREATE UNIQUE NONCLUSTERED INDEX [IX_InvoiceRequest] ON [dbo].[InvoiceRequest] 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[WorkflowServiceInstanceTracking]    Script Date: 12/16/2014 11:45:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkflowServiceInstanceTracking]') AND type in (N'U'))
DROP TABLE [dbo].[WorkflowServiceInstanceTracking]
GO

/****** Object:  Table [dbo].[WorkflowServiceInstanceTracking]    Script Date: 12/16/2014 11:45:12 ******/

CREATE TABLE [dbo].[WorkflowServiceInstanceTracking](
	[WorkflowInstanceId] [uniqueidentifier] NOT NULL,
	[ActivityName] [varchar](250) NULL,
	[ActivityTypeName] [varchar](250) NULL,
	[EventDate] [datetime] NOT NULL,
	[Sequence] [int] NOT NULL,
	[State] [varchar](250) NULL,
	[TrackType] [smallint] NOT NULL
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Teams]    Script Date: 12/05/2014 15:41:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Teams]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Teams](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TeamID] [int] NOT NULL,
	[DeptID] [int] NULL,
	[TeamName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[TeamID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Teams] ON
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (1, 1, 1, N'Team A')
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (2, 2, 1, N'Team B')
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (3, 3, 1, N'Team C')
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (4, 4, 2, N'Team D')
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (5, 5, 2, N'Team E')
INSERT [dbo].[Teams] ([Id], [TeamID], [DeptID], [TeamName]) VALUES (6, 6, 2, N'Team F')
SET IDENTITY_INSERT [dbo].[Teams] OFF
/****** Object:  StoredProcedure [dbo].[SaveActivityStatusHistory]    Script Date: 12/05/2014 15:41:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveActivityStatusHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SaveActivityStatusHistory]
	(		
		@workFlowInstanceId varchar(400),
		@activityType varchar(400),
		@activityName varchar(400),
		@actionTaken varchar(400),
		@description varchar(400),
		@createdByUserId int,
		@referenceFileId varchar(400),
		@dueDate datetime,
		@assignedDate datetime,
		@completedDate datetime,
		@assignedByUserId int,
		@assignedToUserId int,
		@completedUserId int,
		@isFollowUpRequired bit,
		@isCompleted bit,
		@isCancelled bit,
		@isSuccess	bit
	)	
AS

BEGIN
	/* SET NOCOUNT ON */
	
	
		
		INSERT INTO ActivityStatus(WorkFlowInstanceID,ActivityType, ActivityName, ActionTaken, [Description], CreatedUserId, ReferenceFileId,DueDate,AssignedDate,CompletedDate,AssignedByUserId,AssignedToUserId,CompletedUserId,IsCompleted,IsFollowUpRequired,IsCancelled,IsSuccess)
		VALUES (@workFlowInstanceId, @activityType, @activityName, @actionTaken, @description, @createdByUserId,@referenceFileId, @dueDate,@assignedDate,@completedDate,@assignedByUserId,@assignedToUserId,@completedUserId,@isCompleted,@isFollowUpRequired,@isCancelled,@isSuccess)
		
END' 
END
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/05/2014 15:41:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[TeamID] [int] NULL,
	[FirstName] [nvarchar](200) NULL,
	[LastName] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Users] ON
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (1, 1, 1, N'Sachin', N'Gupta', 1)
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (2, 2, 1, N'Kumar', N'Navneet', 1)
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (3, 3, 2, N'sandeep', N'bhatia', 1)
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (4, 4, 2, N'Ankit', N'Shah', 1)
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (6, 5, 4, N'Manis', N'Gupta', 1)
INSERT [dbo].[Users] ([Id], [UserID], [TeamID], [FirstName], [LastName], [IsActive]) VALUES (7, 6, 5, N'Ron', N'Redmer', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  StoredProcedure [dbo].[SaveInvoiceRequest]    Script Date: 12/05/2014 15:41:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveInvoiceRequest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[SaveInvoiceRequest]
	(
		@invoiceId uniqueidentifier,
		@invoiceNumber varchar(200),
		@amount decimal(18,4),
		@creationDate datetime,
		@dueDate datetime,
		@referenceClientId varchar(200),
		@referenceFileId varchar(200),
		@createdBy varchar(200),
		@assignedUserId int,
		@createdByUserId int,
		@description varchar(400),
		@workflowInstanceId uniqueidentifier,
		@isCompleted bit,
		@isDeliveredToClient bit,
		@isSubmittedToAccountIn bit,
		@isCancelled bit	
	)	
AS

BEGIN
	/* SET NOCOUNT ON */
	
	/* if the InvoiceRequest exists... */
	IF exists(SELECT * 
	 		  FROM InvoiceRequest
			  WHERE InvoiceId = @invoiceId) 
			  
	 	/* UPDATE IT */ 
		UPDATE InvoiceRequest SET
			InvoiceNumber = @invoiceNumber,
			Amount = @amount,
			Description = @description,
			ReferenceClientId = @referenceClientId,
			ReferenceFileId = @referenceFileId,
			CreatedBy = @createdBy,
			AssignedUserId = @assignedUserId,
			CreatedByUserId = @createdByUserId,
			WorkflowInstanceId = @workflowInstanceId,
			IsDeliveredToClient = @isDeliveredToClient,
			IsSubmittedToAccountIn = @isSubmittedToAccountIn,
			IsCancelled = @isCancelled,
			IsCompleted = @isCompleted
		WHERE 
		InvoiceId = @invoiceId			  
				
	ELSE /* if it does not exist, insert it */ 
	
	Insert  INTO Workflows (WorkflowID, WorkflowName, IsActive)
		VALUES (@workflowInstanceId,@invoiceNumber,1)
		
		INSERT INTO InvoiceRequest (InvoiceId, InvoiceNumber, Amount,ReferenceClientId,CreatedBy,CreatedDate, IsDeliveredToClient, DueDate,ReferenceFileId,IsSubmittedToAccountIn,AssignedUserId, CreatedByUserId, WorkflowInstanceId,Description,IsCancelled,IsCompleted)
		VALUES (@invoiceId,@invoiceNumber,@amount,@referenceClientId,@createdBy,@creationDate,@isDeliveredToClient,@dueDate,@referenceFileId, @isSubmittedToAccountIn,@assignedUserId,@createdByUserId,@workflowInstanceId,@description,@isCancelled,@isCompleted)
		
END' 
END
GO

/****** Object:  StoredProcedure [dbo].[GetInvoiceByInvoiceId]    Script Date: 12/09/2014 12:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  GetInvoiceByInvoiceId '2B2DE7B6-5FF8-4401-AFF7-8EA88409BF35', ''
CREATE PROCEDURE [dbo].[GetInvoiceByInvoiceId]
@InvoiceID nvarchar(50),
@WorkflowInstanceID nvarchar(50)
AS
BEGIN
IF @InvoiceID <> ''
 BEGIN
  SELECT IR.*, U.FirstName, U.LastName
  FROM dbo.InvoiceRequest IR INNER JOIN Users U
  ON IR.CreatedByUserId = U.UserId
  WHERE InvoiceId = @InvoiceID
 END
ELSE
 BEGIN
  SELECT IR.*, U.FirstName, U.LastName
  FROM dbo.InvoiceRequest IR INNER JOIN Users U
  ON IR.CreatedByUserId = U.UserId
  WHERE WorkflowInstanceId = @WorkflowInstanceID
 END
END
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsAssignedToMe]    Script Date: 12/09/2014 12:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[SelectInvoiceRequestsAssignedToMe]
	(
		@userId int
	)	
AS

BEGIN
	
	
		SELECT ir.InvoiceNumber, ir.InvoiceId,ir.Amount,ir.ReferenceClientId,ir.description,
	ir.createdByUserId,ir.IsSubmittedToAccountIn,ir.IsDeliveredToClient,iat.AssignedUser, 
	case when iat.ActivityType=2 then 1 else 0 end as  IsCompletedFromAccounts 
	FROM InvoiceRequest ir inner join 
	dbo.InvoiceActivityTracking iat on ir.workflowInstanceId = iat.workflowInstanceId 
	inner join (
	select max(workflowTrackingId) workflowTrackingId, workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	from dbo.InvoiceActivityTracking group by workflowInstanceId--,AssignedUser,AssignedTeam, AssignedDepartment, ActivityType 
	) track on iat.workflowTrackingId  = track.workflowTrackingId
	Where iat.AssignedUser=@userId
	order by ir.createdDate
	/*
    SELECT * 
	 	FROM InvoiceRequest
		WHERE AssignedUserId = @userId		  
		ORDER BY CreatedDate
			  */
			  
END
GO
/****** Object:  StoredProcedure [dbo].[SelectInvoiceRequestsStartedByMe]    Script Date: 12/09/2014 12:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
Create PROCEDURE [dbo].[SelectInvoiceRequestsStartedByMe]
	(
		@userId int
	)	
AS

BEGIN
	
	
    SELECT * 
	 	FROM InvoiceRequest
		WHERE CreatedByUserId = @userId		  
		ORDER BY CreatedDate
			  
END
GO
/****** Object:  ForeignKey [FK_Teams_Departments]    Script Date: 12/05/2014 15:41:53 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Teams_Departments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Teams]'))
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_Departments] FOREIGN KEY([DeptID])
REFERENCES [dbo].[Departments] ([DepartmentID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Teams_Departments]') AND parent_object_id = OBJECT_ID(N'[dbo].[Teams]'))
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_Departments]
GO
/****** Object:  ForeignKey [FK_Users_Teams]    Script Date: 12/05/2014 15:41:56 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Teams]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Teams] FOREIGN KEY([TeamID])
REFERENCES [dbo].[Teams] ([TeamID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Users_Teams]') AND parent_object_id = OBJECT_ID(N'[dbo].[Users]'))
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Teams]
GO
