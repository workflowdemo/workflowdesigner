USE [WorkflowService]
GO

/****** Object:  StoredProcedure [dbo].[SaveWorkflowServiceInstanceTracking]    Script Date: 12/16/2014 11:51:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaveWorkflowServiceInstanceTracking]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SaveWorkflowServiceInstanceTracking]
GO

USE [WorkflowService]
GO

/****** Object:  StoredProcedure [dbo].[SaveWorkflowServiceInstanceTracking]    Script Date: 12/16/2014 11:51:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SaveWorkflowServiceInstanceTracking]
	(
		@WorkflowInstanceId uniqueidentifier,
		@ActivityName varchar(250),
		@ActivityTypeName varchar(250),
		@EventDate datetime,
		@Sequence int,
		@State varchar(250),
		@TrackType int
	)	
AS

BEGIN
	/* SET NOCOUNT ON */
	
	INSERT INTO [dbo].[WorkflowServiceInstanceTracking]
           ([WorkflowInstanceId]
           ,[ActivityName]
           ,[ActivityTypeName]
           ,[EventDate]
           ,[Sequence]
           ,[State],[TrackType])
     VALUES
           (@WorkflowInstanceId
           ,@ActivityName
           ,@ActivityTypeName
           ,@EventDate
           ,@Sequence
           ,@State
           ,@TrackType)
		
END
GO

