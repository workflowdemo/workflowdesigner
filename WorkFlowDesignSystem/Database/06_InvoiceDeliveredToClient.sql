USE [WorkflowService]
GO
/****** Object:  StoredProcedure [dbo].[InvoiceDeliveredToClient]    Script Date: 12/18/2014 16:27:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceDeliveredToClient]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InvoiceDeliveredToClient]
GO
/****** Object:  StoredProcedure [dbo].[InvoiceDeliveredToClient]    Script Date: 12/18/2014 16:27:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceDeliveredToClient]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[InvoiceDeliveredToClient]
(		
		@invoiceId varchar(400), 
		@isDeliveredToClient bit,
		@assignedUserID varchar(400)		
)
AS
BEGIN
	UPDATE [WorkflowService].[dbo].[InvoiceRequest] 
	   SET 
	   IsDeliveredToClient = @isDeliveredToClient,
	   AssignedUserId = @assignedUserID	      
 WHERE InvoiceId = @invoiceId
END 


' 
END
GO
