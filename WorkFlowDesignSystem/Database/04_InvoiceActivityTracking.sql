USE [WorkflowService]
GO


/****** Object:  Table [dbo].[InvoiceActivityTracking]    Script Date: 12/08/2014 12:35:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[InvoiceActivityTracking](
	[WorkflowTrackingID] [bigint] IDENTITY(1,1) NOT NULL,
	[WorkflowInstanceID] [uniqueidentifier] NOT NULL,
	[ActivityName] [varchar](50) NOT  NULL,
	[PreviousActivity] [smallint] NULL,
	[NextActivity] [smallint] NULL,
	[ActivityType] [smallint] NOT  NULL,
	[ActionTaken] [smallint] NULL,
	[ReferenceFileID] [varchar](50) NOT NULL,
	[ReferenceClientID] [varchar](50) NOT NULL,
	[ReferenceProcessType] [smallint] NOT NULL,
	[ReferenceProcessID] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedUser] [int] NOT NULL,
	[DueDate] [datetime] NULL,
	[EscalationDays] [smallint] NULL,
	[AssignedDepartment] [smallint] NULL,
	[AssignedTeam] [smallint] NULL,
	[AssignedUser] [int] NULL,
	[AssignedDate] [datetime] NULL,
	[AssignedByUser] [int] NULL,
	[CompletedDate] [datetime] NULL,
	[CompletedUser] [int] NULL,
	[IsFollowUpRequired] [bit] NULL,
	[FollowUpType] [varchar](10) NULL,
	[FollowUpTimeLimit] [varchar](10) NULL,
	[FollowUpTriesLimit] [smallint] NULL,
	[IsCompleted] [bit] NULL,
	[IsCancelled] [bit] NULL,
	[IsSuccess] [bit] NULL,
	[Comments] [varchar](500) NULL,
 CONSTRAINT [PK_InvoiceActivityTracking] PRIMARY KEY CLUSTERED 
(
	[WorkflowTrackingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

