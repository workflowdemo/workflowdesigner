﻿using System;
using System.Collections.Generic;
using System.Linq;
using WorkFlowApplication.Workflow.Activities;
using wfActivity = WorkFlowApplication.Workflow.Activities.Activity;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using Cygrp.Workflow.Core.Extensions;

namespace Cygrp.Workflow.Core
{
    public class WorkflowApplication
    {
        private WorkflowInstance workflowInstance;
        private ActivityContext activityContext;
        IRepository<ActivityStatusRecord> activityTrackingRepository;
        IRepository<Cygrp.Workflow.Core.Model.Activity> activityRepository;

        IWorkflowInstanceRepository _workflowInstanceRepository;
        private readonly IExpressionEvaluator expressionEvaluator;
        private readonly IWorkflowService _workflowService;


  
        public WorkflowApplication(IRepository<ActivityStatusRecord> activityTrackRepo, IRepository<Cygrp.Workflow.Core.Model.Activity> activityRepo,
            IExpressionEvaluator exprEvaluator, IWorkflowService workflowService, IWorkflowInstanceRepository workflowInstanceRepository)
        {
            activityTrackingRepository = activityTrackRepo;
            activityRepository = activityRepo;
            expressionEvaluator = exprEvaluator;
            _workflowService = workflowService;
            _workflowInstanceRepository = workflowInstanceRepository;
        }

        public WorkflowInstance Instance { get { return workflowInstance; } set { workflowInstance = value; } }  

        public void InitialiseWorkflow(Guid processTypeId, Users user)
        {
            var activity = _workflowService.GetFirstProcessActivitiesBy(processTypeId);
            var instance = new WorkflowInstance
            {
                Id = Guid.NewGuid(),
                IsStarted = true,
                ProcessTypeId = processTypeId,
                CurrentActivityId = activity.Id,
                IsEnded = false
            };
            _workflowInstanceRepository.Save(instance);

            this.workflowInstance = instance;

            instance.SetActivityStatus(new ActivityStatusRecord
            {
                DueDate = DateTime.Now,
                Comments = "Initiated",
                CreatedByUserId = user.UserId,
                AssignedToUserId = user.UserId
            });

            var wfActivity  = prepareWorkflowActivity(activity);

            wfActivity.Initiate();
        }

        public void Load(string referenceId)
        {
            workflowInstance = _workflowInstanceRepository.GetByReferenceId(referenceId);
        }

        public WorkflowInstance GetWorkflowInstance(string referenceId)
        {
            if (workflowInstance == null)
            {
                Load(referenceId);
            }
            return workflowInstance;
        }

        public void Run()
        {  
           // workflowInstance.Execute();
            Execute(workflowInstance);
        }

        private wfActivity prepareWorkflowActivity(Cygrp.Workflow.Core.Model.Activity fromActivityEntity)
        {
            prepareActivityContext(fromActivityEntity);

            if (!string.IsNullOrEmpty(fromActivityEntity.Behaviour) && fromActivityEntity.Behaviour.Equals("delay"))
            {
                return new DelayActivity(activityTrackingRepository, activityRepository, expressionEvaluator,_workflowInstanceRepository, _workflowService)
                {
                    ActivityContext = activityContext,
                    Name = fromActivityEntity.Text,
                    Id = fromActivityEntity.Id,
                    Delay = TimeSpan.FromSeconds(10)
                };
            }
            else
            {

                return new wfActivity(activityTrackingRepository, activityRepository, expressionEvaluator, _workflowInstanceRepository, _workflowService)
                {
                    ActivityContext = activityContext,
                    Name = fromActivityEntity.Text,
                    Id = fromActivityEntity.Id
                };
            }
        }

        public void prepareActivityContext(Cygrp.Workflow.Core.Model.Activity fromActivityEntity)
        {
            activityContext = new ActivityContext
            {
                AssignedBy = workflowInstance.ActivityStatus.CreatedByUserId,
                AssignedOn = DateTime.Now,
                AssignedTo = workflowInstance.ActivityStatus.AssignedToUserId,
                DueOn = workflowInstance.ActivityStatus.DueDate,
                WorkflowInstance = this.workflowInstance,
                IsSystemActivity = fromActivityEntity.IsSystemActivity
            };
        }

        private void Execute(WorkflowInstance instance)
        {
            var workflowActivity = prepareWorkflowActivity(activityRepository.Query(i => i.Id.Equals(workflowInstance.CurrentActivityId)).First());

            workflowActivity.Execute(activityContext);
            /*
            // Retrieving next sequencial activity
            var nextActivity = GetNextActivities(workflowActivity);

            // if there is no next sequencial activity, end the workflow
            if (nextActivity == null)
            {
                instance.CurrentActivityId = null;
                instance.Action = "completed";
                instance.IsEnded = true;
                new WorkflowInstanceRepository().Update(instance);
            }
            else
            {

                instance.SetActivityStatus(new ActivityStatusRecord
                {
                    DueDate = DateTime.Now,
                    CreatedByUserId = requestAssignedBy,
                    AssignedToUserId = requestAssignedTo
                });

                var nextWorkflowActivity = prepareWorkflowActivity(nextActivity);
                nextWorkflowActivity.Initiate();

                instance.CurrentActivityId = nextWorkflowActivity.Id;
                // instance.CurrentActivity = nextWorkflowActivity;
                new WorkflowInstanceRepository().Update(instance);
            }
            */
            //     });


            // log instance tracking
            //new InvoiceWorkflowInstanceTrackRepository().Save(new WorkflowInstanceTrack
            //{
            //    WorkflowInstanceId = instance.Id,
            //    ActivityName = workflowActivity.Name,
            //    State = instance.Action,
            //    EventDate = DateTime.Now,
            //    Sequence = 0,
            //    TrackType = 1
            //});
            //****************************************************************



        }

        public void SetAttribute(WorkflowInstance instance, string attributeKey, string attributeValue)
        {
            _workflowService.StoreCustomAttributesfor(instance.Id, instance.CurrentActivityId, attributeKey, attributeValue);
        }
    }
}