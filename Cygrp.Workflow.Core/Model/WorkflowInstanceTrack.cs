﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace Cygrp.Workflow.Core.Model
{
    [Serializable]
    [Table(Name = "WorkflowInstanceTracking")]
    public class WorkflowInstanceTrack
    {
        [Column(IsPrimaryKey=true, IsDbGenerated=true)]
        public int Id { get; set; }
        [Column]
        public Guid WorkflowInstanceId { get; set; }
        [Column]
        public string ActivityName { get; set; }
        [Column]
        public string ActivityTypeName { get; set; }
        [Column]
        public DateTime EventDate { get; set; }
        [Column]
        public int Sequence { get; set; }
        [Column]
        public string State { get; set; }
        [Column]
        // 1- WorkflowInstanceRecord, 2 - ActivityStateRecord, 3 - CustomTrackingRecord
        public int TrackType { get; set; }

    }
}
