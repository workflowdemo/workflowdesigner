﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq.Mapping;

namespace Cygrp.Workflow.Core.Model
{
    [DataContract]
    [Table(Name = "WorkflowActivityTracking")]
    public class ActivityStatusRecord
    {
        [Column(IsPrimaryKey=true, IsDbGenerated=true)]
        public long WorkflowTrackingID { get; set; }

        [DataMember, Column]
        public Guid WorkFlowInstanceID { get; set; }
        
        [DataMember, Column]
        public string ActivityName { get; set; }

        [DataMember,Column]
        public int ActivityType { get; set; }

       [DataMember,Column]
        public int ActionTaken { get; set; }

       [DataMember, Column]
       public string ReferenceProcessID { get; set; }

       [DataMember,Column]
        public DateTime CreatedDate { get; set; }

       [DataMember, Column(Name = "CreatedUser")]
       public int CreatedByUserId { get; set; }

       [DataMember,Column]
        public DateTime DueDate { get; set; }

       [DataMember,Column]
        public DateTime AssignedDate { get; set; }

       [DataMember,Column]
        public DateTime? CompletedDate { get; set; }

       [DataMember, Column(Name = "AssignedByUser")]
        public int AssignedByUserId { get; set; }

       [DataMember, Column(Name = "AssignedUser")]
        public int AssignedToUserId { get; set; }

       
       [DataMember,Column(Name="CompletedUser")]
        public int? CompletedUserId { get; set; }

        [DataMember,Column]
        public int EscalationDays { get; set; }

       [DataMember,Column]
        public string Comments { get; set; }

        [Column]
       public string ActivityId { get; set; }

    }
}
