﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.Serialization;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "Teams")]
    [DataContract]
    public class Teams
    {
        //private EntityRef<Users> _user;

        [Column(DbType = "Int", IsPrimaryKey=true)]
        [DataMember]
        public int TeamID { get; set; }

        [Column]
        [DataMember]
        public int DeptID { get; set; }

        [Column]
        [DataMember]
        public string TeamName { get; set; }
    }
}





