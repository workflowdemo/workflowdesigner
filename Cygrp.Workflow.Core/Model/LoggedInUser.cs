﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    public class LoggedInUser
    {
        public string SessionId { get; set; }

        public int Name { get; set; }

    }
}