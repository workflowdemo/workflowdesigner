﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name="WorkflowProcesses")]
    public class WorkflowProcess
    {
        [Column(IsPrimaryKey=true)]
        public Guid WorkflowProcessID { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string Description { get; set; }
        public IEnumerable<Activity> Shapes { get; set; }
       
    }
}