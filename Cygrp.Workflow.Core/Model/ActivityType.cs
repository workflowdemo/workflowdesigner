﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    public enum ActivityType
    {
        Start = 0,
        Process = 1,
        Decisive = 3,
        End = 10
    }
}