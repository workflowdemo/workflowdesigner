﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "ProcessActivityAttribute")]
    public class ProcessActivityAttribute
    {
        [Column(IsPrimaryKey = true)]
        public Guid Id { get; set; }
        [Column]
        public string ActivityID { get; set; }
        [Column]
        public Guid WorkFlowProcessId { get; set; }
        [Column(Name="AttributeId")]
        public string AttributeKey { get; set; }
        [Column]
        public string AttributeValue { get; set; }
        [Column]
        public string AttributeType { get; set; }
    }
}