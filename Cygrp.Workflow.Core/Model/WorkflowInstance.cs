﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.Mapping;
using WorkFlowApplication.Workflow.Activities;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "WorkflowInstance")]
    public class WorkflowInstance
    {
        [Column(IsPrimaryKey = true)]
        public Guid Id { get; set; }
        [Column]
        public Guid ProcessTypeId { get; set; }
        [Column]
        public string ReferenceId { get; set; }
        [Column]
        public bool IsStarted { get; set; }
        [Column]
        public string Action { get; set; }

        [Column]
        public string CurrentActivityId { get; set; }

        [Column]
        public bool IsEnded { get; set; }

        public ActivityStatusRecord ActivityStatus { get; set; }

    }
}