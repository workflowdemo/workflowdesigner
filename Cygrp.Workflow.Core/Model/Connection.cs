﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name="Connections")]
    public class Connection
    {
        [Column(IsPrimaryKey=true)]
        public Guid ConnectionID { get; set; }
        [Column]
        public string ShapeID { get; set; }
      //  [ForeignKey("ShapeID")]
        //public Shape Shape { get; set; }
        [Column]
        public string Text { get; set; }
        [Column]
        public string TargetId { get; set; }
        [Column]
        public string SourceAnchorType { get; set; }
        [Column]
        public string TargetAnchorType { get; set; }

    }
}