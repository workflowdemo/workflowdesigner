﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "Activities")]
    public class Activity
    {
        [Column(IsPrimaryKey=true)]
        public string Id { get; set; }
        [Column]
        public Guid WorkflowProcessID { get; set; }
       //  [ForeignKey("WorkflowProcessID")]
       // public WorkflowProcess Process { get; set; }
        [Column]        
        public string Text { get; set; }
        [Column]
        public ActivityType Type { get; set; }
        [Column]
        public string TypeText { get; set; }
        [Column]
        public int positionX { get; set; }
        [Column]
        public int positionY { get; set; }
        [Column]
        public int Width { get; set; }
        [Column]
        public int Height { get; set; }

        [Column]
        public int Sequence { get; set; }
        [Column]
        public string Name { get; set; }

        public IEnumerable<Connection> Connections { get; set; }

        public void SetShapeType() {
            ActivityType _value = ActivityType.Start;
            if (TypeText.ToLower().Equals("startpoint"))
            {
                _value = ActivityType.Start;
            }
            if (TypeText.ToLower().Equals("decision"))
            {
                _value = ActivityType.Decisive;
            }
            if (TypeText.ToLower().Equals("task"))
            {
                _value = ActivityType.Process;
            }
            if (TypeText.ToLower().Equals("endpoint"))
            {
                _value = ActivityType.End;
            }
            this.Type= _value;
        }

        public IEnumerable<ProcessActivityAttribute> CustomAttributes { get; set; }
       // public IEnumerable<ProcessActivityAttribute> AddcustomAttributeItems { get; set; }

        [Column(Name = "Behaviour")]
        public string Behaviour { get; set; }

        [Column]
        public char? ExecutingUserType { get; set; }

        public bool IsUserActivity
        {
            get {
                if (ExecutingUserType.HasValue && ExecutingUserType.Value.ToString().ToUpper().Equals("U")) {
                    return true;
                }
                return false;
            }
        }

        public bool IsSystemActivity
        {
            get
            {
                if (ExecutingUserType.HasValue && ExecutingUserType.Value.ToString().ToUpper().Equals("S"))
                {
                    return true;
                }
                return false;
            }
        }
    }

}
