﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.Serialization;
using System.Data.Linq.Mapping;
using System.Data.Linq;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "Users")]
    [DataContract]
    public class Users
    {
        [Column(DbType = "Int", Name = "UserID")]
        [DataMember]
        public int UserId { get; set; }

        [Column(Name = "TeamID")]
        [DataMember]
        public int TeamID { get; set; }

        //[Association(Name = "Users_Teams", ThisKey = "TeamID")]
        //public EntitySet<Teams> Teams { get; set; }

        [Column]
        [DataMember]
        public string FirstName { get; set; }

        [Column]
        [DataMember]
        public string LastName { get; set; }

        [Column]
        [DataMember]
        public bool IsActive { get; set; }

        public string FullName { get { return FirstName + " " + LastName; } }

    }
}


