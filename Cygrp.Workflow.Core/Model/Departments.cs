﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Runtime.Serialization;
using System.Data.Linq.Mapping;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "Departments")]
    [DataContract]
    public class Departments
    {

        [Column(DbType = "Int")]
        [DataMember]
        public int DepartmentID { get; set; }

        [Column]
        [DataMember]
        public string DepartmentName { get; set; }
    }
}







