﻿using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Model
{
    [Table(Name = "WorkflowInstanceAttributes")]
    public class WorkflowInstanceAttribute
    {
        [Column(IsPrimaryKey=true)]
        public Guid Id { get; set; }

        [Column]
        public Guid WorkflowInstanceId { get; set; }

        [Column]
        public Guid ActivityAttributeId { get; set; }

        [Column]
        public string AttributeKey { get; set; }

        [Column]
        public string AttributeValue { get; set; }
    }
}