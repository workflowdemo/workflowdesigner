﻿using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;
using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;

namespace WorkFlowApplication.Workflow.Activities
{
    public class DelayActivity: Activity
    {
        public DelayActivity(IRepository<ActivityStatusRecord> repo, IRepository<Cygrp.Workflow.Core.Model.Activity> activityRepo,
            IExpressionEvaluator exprEvaluator, IWorkflowInstanceRepository workflowInstanceRepository, IWorkflowService workflowService)
            : base(repo, activityRepo, exprEvaluator, workflowInstanceRepository, workflowService)
        { 
        }

        public TimeSpan Delay { get; set; }

        public override void Initiate()
        {
            base.Initiate();
            BackgroundJob.Schedule(() => action(this.ActivityContext), Delay);
        }


        public void action(ActivityContext context)
        {
            Execute(context);
        }
    }
}