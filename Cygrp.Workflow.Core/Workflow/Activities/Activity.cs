﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cygrp.Workflow.Core.Extensions;
using Cygrp.Workflow.Core.Interface;
using Cygrp.Workflow.Core.Model;


namespace WorkFlowApplication.Workflow.Activities
{
    public class Activity: IActivity
    {

        private readonly IRepository<ActivityStatusRecord> activityTrackingRepository;
        private readonly IRepository<Cygrp.Workflow.Core.Model.Activity> activityRepository;
        private readonly IExpressionEvaluator expressionEvaluator;
        private readonly IWorkflowInstanceRepository _workflowInstanceRepository;
        private readonly IWorkflowService _workflowService;
        public Activity(IRepository<ActivityStatusRecord> activityTrackingRepo, IRepository<Cygrp.Workflow.Core.Model.Activity> activityRepo,
           IExpressionEvaluator exprEvaluator, IWorkflowInstanceRepository workflowInstanceRepository, IWorkflowService workflowService)
        {
            activityTrackingRepository = activityTrackingRepo;
            activityRepository = activityRepo;
            expressionEvaluator = exprEvaluator;
            _workflowInstanceRepository = workflowInstanceRepository;
            _workflowService = workflowService;
        }

        public ActivityContext ActivityContext { get; set; }

        public virtual void Initiate()
        {
            var activityStatus = new ActivityStatusRecord
            {
                ActivityId = Id,
                WorkFlowInstanceID = ActivityContext.WorkflowInstance.Id,
                ActivityName = Name,
                AssignedByUserId = ActivityContext.AssignedBy,
                AssignedDate = ActivityContext.AssignedOn,
                CreatedByUserId = ActivityContext.AssignedBy,
                CreatedDate = ActivityContext.AssignedOn,
                DueDate = ActivityContext.DueOn,
                AssignedToUserId = (ActivityContext.IsSystemActivity ? 1000 : ActivityContext.AssignedTo),
                ActivityType = 0
            };

            activityTrackingRepository.Save(activityStatus);
           
        }

        public virtual void Execute(ActivityContext context)
        {
            var inprogressActivityTracker = activityTrackingRepository
               .Query(i => i.WorkFlowInstanceID.Equals(context.WorkflowInstance.Id))
               .Where(i => i.ActivityId.Equals(context.WorkflowInstance.CurrentActivityId))
               .First();

            if (inprogressActivityTracker != null)
            {
                inprogressActivityTracker.CompletedUserId = (context.IsSystemActivity ? 1000 : context.AssignedBy);
                inprogressActivityTracker.CompletedDate = DateTime.Now;
                activityTrackingRepository.Update(inprogressActivityTracker);
                activityTrackingRepository.Save();
            }

            var activityEntity = activityRepository.Query(i => i.Id.Equals(context.WorkflowInstance.CurrentActivityId)).First();

            var workflowActivity = prepareWorkflowActivity(activityEntity, context);


            var nextActivity = _workflowService.GetNextWorkflowActivity(activityEntity, context.WorkflowInstance.Id);

            // if there is no next sequencial activity, end the workflow
            if (nextActivity == null)
            {
                context.WorkflowInstance.CurrentActivityId = null;
                context.WorkflowInstance.Action = "completed";
                context.WorkflowInstance.IsEnded = true;
                _workflowInstanceRepository.Update(context.WorkflowInstance);
            }
            else
            {

                context.WorkflowInstance.SetActivityStatus(new ActivityStatusRecord
                {
                    DueDate = DateTime.Now,
                    CreatedByUserId = context.AssignedBy,
                    AssignedToUserId = context.AssignedTo
                });
                context.AssignedBy = (context.IsSystemActivity ? 1000 : context.AssignedBy);

                context.WorkflowInstance.CurrentActivityId = nextActivity.Id;
                context.IsSystemActivity = nextActivity.IsSystemActivity;

                var nextWorkflowActivity = prepareWorkflowActivity(nextActivity, context);
                nextWorkflowActivity.Initiate();

                // instance.CurrentActivity = nextWorkflowActivity;
                _workflowInstanceRepository.Update(context.WorkflowInstance);
            }

          
        }

        private Activity prepareWorkflowActivity(Cygrp.Workflow.Core.Model.Activity fromActivityEntity, ActivityContext context)
        {

            if (!string.IsNullOrEmpty(fromActivityEntity.Behaviour) && fromActivityEntity.Behaviour.Equals("delay"))
            {
                return new DelayActivity(activityTrackingRepository, activityRepository, expressionEvaluator, _workflowInstanceRepository, _workflowService)
                {
                    ActivityContext = context,
                    Name = fromActivityEntity.Text,
                    Id = fromActivityEntity.Id,
                    Delay = _workflowService.GetDelayTimeSpanForActivity(fromActivityEntity,context.WorkflowInstance.Id)
                };
            }
            else
            {

                return new Activity(activityTrackingRepository, activityRepository, expressionEvaluator, _workflowInstanceRepository, _workflowService)
                {
                    ActivityContext = context,
                    Name = fromActivityEntity.Text,
                    Id = fromActivityEntity.Id
                };
            }
        }

        public string Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
    }
}