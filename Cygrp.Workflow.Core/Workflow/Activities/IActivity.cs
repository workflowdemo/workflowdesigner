﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkFlowApplication.Workflow.Activities
{
    public interface IActivity
    {
        string Id { get; set; }

        string Name { get; set; }

        void Execute(ActivityContext context);

        void Initiate();
    }
}
