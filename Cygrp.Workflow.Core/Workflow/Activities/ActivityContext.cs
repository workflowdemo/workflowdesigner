﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WorkFlowApplication.Workflow.Activities
{
    public class ActivityContext
    {
        public WorkflowInstance WorkflowInstance { get; set; }

        public string ReferenceProcessId { get; set; }
        public int AssignedBy { get; set; }
        public int AssignedTo { get; set; }
        public DateTime AssignedOn { get; set; }
        public DateTime DueOn { get; set; }
        public bool IsSystemActivity { get; set; }
    }
}