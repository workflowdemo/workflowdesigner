﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WorkFlowApplication.Workflow;

namespace Cygrp.Workflow.Core.Extensions
{
    public static class WorkflowInstanceExtension
    {
        
        public static void SetActivityStatus(this WorkflowInstance instance, ActivityStatusRecord activityStatus)
        {
            if (instance.ActivityStatus == null)
            {
                instance.ActivityStatus = new ActivityStatusRecord();
            }
            instance.ActivityStatus.WorkFlowInstanceID = instance.Id;
            //instance.ActivityStatus.ReferenceProcessID = instance.ReferenceId;
            instance.ActivityStatus.CreatedDate = DateTime.Now;
            instance.ActivityStatus.AssignedDate = DateTime.Now;
           // instance.ActivityStatus.ReferenceProcessType = 1;
            instance.ActivityStatus.ActionTaken = 1; //activityStatus.ActionTaken;


            instance.ActivityStatus.DueDate = activityStatus.DueDate;
            instance.ActivityStatus.CreatedByUserId = activityStatus.CreatedByUserId;
            instance.ActivityStatus.AssignedByUserId = activityStatus.CreatedByUserId;
            instance.ActivityStatus.AssignedToUserId = activityStatus.AssignedToUserId;
            instance.ActivityStatus.Comments = activityStatus.Comments;
        }

    }
}