﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAll();

        T GetBy(string name);

        IEnumerable<T> Query(Expression<Func<T, bool>> filter);

        void Add(T entity);

        T Save(T entity);

        void Update(T entity);

        void Remove(T entity);

        void Save();
    }

}
