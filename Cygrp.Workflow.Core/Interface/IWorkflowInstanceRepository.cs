﻿using Cygrp.Workflow.Core.Model;
using System;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IWorkflowInstanceRepository
    {
        WorkflowInstance GetByReferenceId(string referenceId);
        void Save(WorkflowInstance workflowInstance);
        void Update(WorkflowInstance workflowInstance);
    }
}
