﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IUserSessionRepository
    {
        Users GetLoggedInUser();

        void SetUserSession(string userKey);
    }
}
