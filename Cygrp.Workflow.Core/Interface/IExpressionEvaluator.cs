﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IExpressionEvaluator
    {
        T Evaluate<T>(string stringExpression) where T : class ;

        DateTime Evaluate(string stringExpression);
    }
}