﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IWorkflowService
    {
         IEnumerable<WorkflowProcess> GetAll();
        
         void Save(WorkflowProcess process);

         WorkflowProcess Get(string name);

         Activity GetFirstProcessActivitiesBy(Guid workflowProcessId);

        Activity GetNextWorkflowActivity(Activity currentActivity, Guid workflowInstanceId);

        TimeSpan GetDelayTimeSpanForActivity(Activity fromActivityEntity, Guid workflowInstanceId);

        void StoreCustomAttributesfor(Guid workflowInstanceId, string activityId, string attributeKey, string attributeValue);

    }

}