﻿using Cygrp.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cygrp.Workflow.Core.Interface
{
    public interface IQueryUserService
    {
        Users GetUser(int userId);

        IList<Users> GetAllUsers();
    }
}
